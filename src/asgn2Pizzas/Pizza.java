package asgn2Pizzas;

import static java.time.temporal.ChronoUnit.MINUTES;

import java.time.LocalTime;

import asgn2Exceptions.PizzaException;

/**
 * An abstract class that represents pizzas sold at the Pizza Palace restaurant.
 * The Pizza class is used as a base class of VegetarianPizza, MargheritaPizza
 * and MeatLoversPizza. Each of these subclasses have a different set of
 * toppings. A description of the class's fields and their constraints is
 * provided in Section 5.1 of the Assignment Specification.
 * 
 * @author Sylvester Soetianto
 *
 */
public abstract class Pizza {
	private final int quantity;
	private final LocalTime orderTime;
	private final LocalTime deliveryTime;
	private final String type;
	private final double price;
	private double cost;
	static final String MARGHERITA = "Margherita";
	static final String VEGETARIAN = "Vegetarian";
	static final String MEATLOVERS = "Meat Lovers";
	static private final int MIN_PIZZA = 1, MAX_PIZZA = 10;
	static private final LocalTime OPEN_PIZZA = LocalTime.of(19, 0), CLOSE_PIZZA = LocalTime.of(23, 0);
	static final double PZM_PRICE = 8.00;
	static final double PZV_PRICE = 10.00;
	static final double PZL_PRICE = 12.00;

	/**
	 * This class represents a pizza produced at the Pizza Palace restaurant. A
	 * detailed description of the class's fields and parameters is provided in
	 * the Assignment Specification, in particular in Section 5.1. A
	 * PizzaException is thrown if the any of the constraints listed in Section
	 * 5.1 of the Assignment Specification are violated.
	 *
	 * PRE: TRUE POST: All field values except cost per pizza are set
	 * 
	 * @param quantity
	 *            - The number of pizzas ordered
	 * @param orderTime
	 *            - The time that the pizza order was made and sent to the
	 *            kitchen
	 * @param deliveryTime
	 *            - The time that the pizza was delivered to the customer
	 * @param type
	 *            - A human understandable description of this Pizza type
	 * @param price
	 *            - The price that the pizza is sold to the customer
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * 
	 */
	public Pizza(int quantity, LocalTime orderTime, LocalTime deliveryTime, String type, double price)
			throws PizzaException {
		this.quantity = quantity;
		this.orderTime = orderTime;
		this.deliveryTime = deliveryTime;
		this.type = type.trim();
		this.price = price;

		// Verify Quantity (1 <= Quantity <= 10)
		if (this.quantity < MIN_PIZZA) {
			throw new PizzaException("Insufficient pizza quantity");
		} else if (this.quantity > MAX_PIZZA) {
			throw new PizzaException("Excessive pizza quantity");
		}

		// Verify Order Time (19:00:00 <= Order Time <= 23:00:00)
		if (this.orderTime.isBefore(OPEN_PIZZA)) {
			throw new PizzaException("Kitchen has not opened yet");
		} else if (this.orderTime.isAfter(CLOSE_PIZZA)) {
			throw new PizzaException("Kitchen has closed already");
		}

		// Verify Deliver Time (Order Time + 00:10:00 <= Deliver Time <= Order
		// Time + 01:10:00)
		if (this.orderTime.until(this.deliveryTime, MINUTES) > 70) {
			throw new PizzaException("Pizza is not fresh anymore and has been thrown out");
		} else if (this.orderTime.until(this.deliveryTime, MINUTES) < 10
				&& this.deliveryTime.isAfter(LocalTime.MIDNIGHT.plusMinutes(10))) {
			throw new PizzaException("Pizza has not finished cooking yet");
		}

		// Verify Type (MARGHERITA || VEGETARIAN || MEATLOVERS)
		if (!(this.type.equals(MARGHERITA) || this.type.equals(VEGETARIAN) || this.type.equals(MEATLOVERS))) {
			throw new PizzaException("Not a valid type of Pizza");
		}

		// Verify Price
		if (!((this.type.equals(MARGHERITA) && this.price == PZM_PRICE)
				|| (this.type.equals(VEGETARIAN) && this.price == PZV_PRICE)
				|| (this.type.equals(MEATLOVERS) && this.price == PZL_PRICE))) {
			throw new PizzaException("Price not equivalent to order");
		}
	}

	/**
	 * Calculates how much a pizza would could to make calculated from its
	 * toppings.
	 * 
	 * <P>
	 * PRE: TRUE
	 * <P>
	 * POST: The cost field is set to sum of the Pizzas's toppings
	 */
	public final void calculateCostPerPizza() {
		switch (type) {
		case MARGHERITA:
			cost = PizzaTopping.TOMATO.getCost() + PizzaTopping.CHEESE.getCost();
			break;
		case VEGETARIAN:
			cost = PizzaTopping.TOMATO.getCost() + PizzaTopping.CHEESE.getCost() + PizzaTopping.EGGPLANT.getCost()
					+ PizzaTopping.MUSHROOM.getCost() + PizzaTopping.CAPSICUM.getCost();
			break;
		case MEATLOVERS:
			cost = PizzaTopping.TOMATO.getCost() + PizzaTopping.CHEESE.getCost() + PizzaTopping.BACON.getCost()
					+ PizzaTopping.PEPPERONI.getCost() + PizzaTopping.SALAMI.getCost();
			break;
		}
	}

	/**
	 * Returns the amount that an individual pizza costs to make.
	 * 
	 * @return The amount that an individual pizza costs to make.
	 */
	public final double getCostPerPizza() {
		calculateCostPerPizza();
		return cost;
	}

	/**
	 * Returns the amount that an individual pizza is sold to the customer.
	 * 
	 * @return The amount that an individual pizza is sold to the customer.
	 */
	public final double getPricePerPizza() {
		double price = 0;
		switch (type) {
		case MARGHERITA:
			price = PZM_PRICE;
			break;
		case VEGETARIAN:
			price = PZV_PRICE;
			break;
		case MEATLOVERS:
			price = PZL_PRICE;
			break;
		}
		return price;
	}

	/**
	 * Returns the amount that the entire order costs to make, taking into
	 * account the type and quantity of pizzas.
	 * 
	 * @return The amount that the entire order costs to make, taking into
	 *         account the type and quantity of pizzas.
	 */
	public final double getOrderCost() {
		return quantity * getCostPerPizza();
	}

	/**
	 * Returns the amount that the entire order is sold to the customer, taking
	 * into account the type and quantity of pizzas.
	 * 
	 * @return The amount that the entire order is sold to the customer, taking
	 *         into account the type and quantity of pizzas.
	 */
	public final double getOrderPrice() {
		return quantity * getPricePerPizza();
	}

	/**
	 * Returns the profit made by the restaurant on the order which is the order
	 * price minus the order cost.
	 * 
	 * @return Returns the profit made by the restaurant on the order which is
	 *         the order price minus the order cost.
	 */
	public final double getOrderProfit() {
		return getOrderPrice() - getOrderCost();
	}

	/**
	 * Indicates if the pizza contains the specified pizza topping or not.
	 * 
	 * @param topping
	 *            - A topping as specified in the enumeration PizzaTopping
	 * @return Returns true if the instance of Pizza contains the specified
	 *         topping and false otherwise.
	 */
	public final boolean containsTopping(PizzaTopping topping) {
		return type.equals(MARGHERITA) && (topping.equals(PizzaTopping.TOMATO) || topping.equals(PizzaTopping.CHEESE))
				|| type.equals(VEGETARIAN) && (topping.equals(PizzaTopping.TOMATO)
						|| topping.equals(PizzaTopping.CHEESE) || topping.equals(PizzaTopping.EGGPLANT)
						|| topping.equals(PizzaTopping.MUSHROOM) || topping.equals(PizzaTopping.CAPSICUM))
				|| type.equals(MEATLOVERS) && (topping.equals(PizzaTopping.TOMATO)
						|| topping.equals(PizzaTopping.CHEESE) || topping.equals(PizzaTopping.BACON)
						|| topping.equals(PizzaTopping.PEPPERONI) || topping.equals(PizzaTopping.SALAMI));
	}

	/**
	 * Returns the quantity of pizzas ordered.
	 * 
	 * @return the quantity of pizzas ordered.
	 */
	public final int getQuantity() {
		return quantity;
	}

	/**
	 * Returns a human understandable description of the Pizza's type. The valid
	 * alternatives are listed in Section 5.1 of the Assignment Specification.
	 * 
	 * @return A human understandable description of the Pizza's type.
	 */
	public final String getPizzaType() {
		return type;
	}

	/**
	 * Compares *this* Pizza object with an instance of an *other* Pizza object
	 * and returns true if if the two objects are equivalent, that is, if the
	 * values exposed by public methods are equal. You do not need to test this
	 * method.
	 * 
	 * @return true if *this* Pizza object and the *other* Pizza object have the
	 *         same values returned for getCostPerPizza(), getOrderCost(),
	 *         getOrderPrice(), getOrderProfit(), getPizzaType(),
	 *         getPricePerPizza() and getQuantity().
	 * 
	 */
	@Override
	public boolean equals(Object other) {
		Pizza otherPizza = (Pizza) other;
		return ((this.getCostPerPizza()) == (otherPizza.getCostPerPizza())
				&& (this.getOrderCost()) == (otherPizza.getOrderCost()))
				&& (this.getOrderPrice()) == (otherPizza.getOrderPrice())
				&& (this.getOrderProfit()) == (otherPizza.getOrderProfit())
				&& (this.getPizzaType() == (otherPizza.getPizzaType())
						&& (this.getPricePerPizza()) == (otherPizza.getPricePerPizza())
						&& (this.getQuantity()) == (otherPizza.getQuantity()));
	}

}
