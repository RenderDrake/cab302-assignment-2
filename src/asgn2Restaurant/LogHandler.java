package asgn2Restaurant;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;

import asgn2Customers.Customer;
import asgn2Customers.DriverDeliveryCustomer;
import asgn2Customers.DroneDeliveryCustomer;
import asgn2Customers.PickUpCustomer;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.MargheritaPizza;
import asgn2Pizzas.MeatLoversPizza;
import asgn2Pizzas.Pizza;
import asgn2Pizzas.VegetarianPizza;

/**
 *
 * A class that contains methods that use the information in the log file to
 * return Pizza and Customer object - either as an individual Pizza/Customer
 * object or as an ArrayList of Pizza/Customer objects.
 *
 * @author Sylvester Soetianto and Charles Alleman
 *
 */
public class LogHandler {

	static private final String PICKUP_CODE = "PUC", DRONE_CODE = "DNC", DRIVER_CODE = "DVC", MARGHERITA = "PZM",
			VEGETARIAN = "PZV", MEATLOVERS = "PZL";
	static private final int ORDER_TIME = 0, DELIVER_TIME = 1, NAME = 2, PHONE_NUMBER = 3, CUSTOMER_CODE = 4,
			LOCATION_X = 5, LOCATION_Y = 6, PIZZA_CODE = 7, QUANTITY = 8;
	private static BufferedReader bufferedReader;
	private static FileReader fileReader;
	private static int customerLineNo = 0;
	private static int pizzaLineNo = 0;

	/**
	 * Returns an ArrayList of Customer objects from the information contained
	 * in the log file ordered as they appear in the log file.
	 *
	 * @param filename
	 *            The file name of the log file
	 * @return an ArrayList of Customer objects from the information contained
	 *         in the log file ordered as they appear in the log file.
	 * @throws CustomerException
	 *             If the log file contains semantic errors leading that violate
	 *             the customer constraints listed in Section 5.3 of the
	 *             Assignment Specification or contain an invalid customer code
	 *             (passed by another class).
	 * @throws LogHandlerException
	 *             If there was a problem with the log file not related to the
	 *             semantic errors above
	 *
	 */
	public static ArrayList<Customer> populateCustomerDataset(String filename)
			throws CustomerException, LogHandlerException {
		final int MIN_SIZE_FILENAME = 5;
		final int FILE_TYPE_DELIM = 4;
		final String TEXT_FILE = ".txt";

		ArrayList<Customer> customers = new ArrayList<>();
		String line;

		if (filename.length() < MIN_SIZE_FILENAME
				|| !filename.substring(filename.length() - FILE_TYPE_DELIM).equalsIgnoreCase(TEXT_FILE)) {
			throw new LogHandlerException("File is Not A Text File (.txt).");
		}

		try {
			// FileReader reads text files in the default encoding.
			fileReader = new FileReader(filename);

			// Always wrap FileReader in BufferedReader.
			bufferedReader = new BufferedReader(fileReader);

			boolean isEmpty = true;

			while ((line = bufferedReader.readLine()) != null) {
				isEmpty = false;
				customers.add(createCustomer(line));
			}

			if (isEmpty) {
				throw new LogHandlerException("File Empty");
			}

			// Always close files.
			bufferedReader.close();
		} catch (FileNotFoundException ex) {
			throw new LogHandlerException("Unable To Open File '" + filename + "'.");
		} catch (IOException ex) {
			throw new LogHandlerException("Error Reading File '" + filename + "'.");
		}

		return customers;
	}

	/**
	 * Returns an ArrayList of Pizza objects from the information contained in
	 * the log file ordered as they appear in the log file. .
	 *
	 * @param filename
	 *            The file name of the log file
	 * @return an ArrayList of Pizza objects from the information contained in
	 *         the log file ordered as they appear in the log file. .
	 * @throws PizzaException
	 *             If the log file contains semantic errors leading that violate
	 *             the pizza constraints listed in Section 5.3 of the Assignment
	 *             Specification or contain an invalid pizza code (passed by
	 *             another class).
	 * @throws LogHandlerException
	 *             If there was a problem with the log file not related to the
	 *             semantic errors above
	 *
	 */
	public static ArrayList<Pizza> populatePizzaDataset(String filename) throws PizzaException, LogHandlerException {
		final int MIN_SIZE_FILENAME = 5;
		final int FILE_TYPE_DELIM = 4;
		final String TEXT_FILE = ".txt";

		ArrayList<Pizza> pizzas = new ArrayList<>();
		String line;

		if (filename.length() < MIN_SIZE_FILENAME
				|| !filename.substring(filename.length() - FILE_TYPE_DELIM).equalsIgnoreCase(TEXT_FILE)) {
			throw new LogHandlerException("File is Not A Text File (.txt).");
		}

		try {
			// FileReader reads text files in the default encoding.
			fileReader = new FileReader(filename);

			// Always wrap FileReader in BufferedReader.
			bufferedReader = new BufferedReader(fileReader);

			boolean isEmpty = true;

			while ((line = bufferedReader.readLine()) != null) {
				isEmpty = false;
				pizzas.add(createPizza(line));
			}

			if (isEmpty) {
				throw new LogHandlerException("File Empty.");
			}

			// Always close files.
			bufferedReader.close();
		} catch (FileNotFoundException ex) {
			throw new LogHandlerException("Unable To Open File '" + filename + "'.");
		} catch (IOException ex) {
			throw new LogHandlerException("Error Reading File '" + filename + "'.");
		}

		return pizzas;
	}

	/**
	 * Creates a Customer object by parsing the information contained in a
	 * single line of the log file. The format of each line is outlined in
	 * Section 5.3 of the Assignment Specification.
	 *
	 * @param line
	 *            - A line from the log file
	 * @return - A Customer object containing the information from the line in
	 *         the log file
	 * @throws CustomerException
	 *             - If the log file contains semantic errors leading that
	 *             violate the customer constraints listed in Section 5.3 of the
	 *             Assignment Specification or contain an invalid customer code
	 *             (passed by another class).
	 * @throws LogHandlerException
	 *             - If there was a problem parsing the line from the log file.
	 */
	public static Customer createCustomer(String line) throws CustomerException, LogHandlerException {
		String[] elements = line.split(",");
		customerLineNo++;

		// Check for correct number of elements
		if (elements.length != 9) {
			throw new LogHandlerException("Invalid Format at line " + Integer.toString(customerLineNo) + ".");
		}

		// Check no field is empty or contains whitespace before or after the
		// string
		for (int i = 0; i < 9; i++) {
			if (elements[i].length() == 0 || line.trim().length() != line.length()) {
				throw new LogHandlerException("Invalid Format at line " + Integer.toString(customerLineNo) + ".");
			}
		}

		// Check valid order time format
		try {
			LocalTime.parse(elements[ORDER_TIME]);
		} catch (Exception e) {
			throw new LogHandlerException("Invalid Order Time at line " + Integer.toString(customerLineNo) + ".");
		}

		// Check valid delivery time format
		try {
			LocalTime.parse(elements[DELIVER_TIME]);
		} catch (Exception e) {
			throw new LogHandlerException("Invalid Deliver Time at line " + Integer.toString(customerLineNo) + ".");
		}

		// Check valid phone number length and values are only numbers with 0
		// starting
		if (elements[PHONE_NUMBER].length() != 10 || !elements[PHONE_NUMBER].matches("^0[0-9]*$")) {
			throw new LogHandlerException("Invalid Phone Number at line " + Integer.toString(customerLineNo) + ".");
		}

		// Check location is a number
		try {
			Integer.parseInt(elements[LOCATION_X]);
			Integer.parseInt(elements[LOCATION_Y]);
		} catch (Exception e) {
			throw new LogHandlerException("Invalid Location at line " + Integer.toString(customerLineNo) + ".");
		}

		// Check Quantity is a number
		try {
			Integer.parseInt(elements[QUANTITY]);
		} catch (Exception e) {
			throw new LogHandlerException("Invalid Quantity at line " + Integer.toString(customerLineNo) + ".");
		}

		try {
			// Create customer
			switch (elements[CUSTOMER_CODE]) {
			case PICKUP_CODE:
				return new PickUpCustomer(elements[NAME], elements[PHONE_NUMBER],
						Integer.parseInt(elements[LOCATION_X]), Integer.parseInt(elements[LOCATION_Y]));
			case DRONE_CODE:
				return new DroneDeliveryCustomer(elements[NAME], elements[PHONE_NUMBER],
						Integer.parseInt(elements[LOCATION_X]), Integer.parseInt(elements[LOCATION_Y]));
			case DRIVER_CODE:
				return new DriverDeliveryCustomer(elements[NAME], elements[PHONE_NUMBER],
						Integer.parseInt(elements[LOCATION_X]), Integer.parseInt(elements[LOCATION_Y]));
			}
		} catch (CustomerException e) {
			throw new CustomerException(e.getMessage() + " at line " + Integer.toString(customerLineNo) + ".");
		}

		// Failed to find valid code
		throw new CustomerException("Customer Code Invalid at line " + Integer.toString(customerLineNo) + ".");
	}

	/**
	 * Creates a Pizza object by parsing the information contained in a single
	 * line of the log file. The format of each line is outlined in Section 5.3
	 * of the Assignment Specification.
	 *
	 * @param line
	 *            - A line from the log file
	 * @return - A Pizza object containing the information from the line in the
	 *         log file
	 * @throws PizzaException
	 *             If the log file contains semantic errors leading that violate
	 *             the pizza constraints listed in Section 5.3 of the Assignment
	 *             Specification or contain an invalid pizza code (passed by
	 *             another class).
	 * @throws LogHandlerException
	 *             - If there was a problem parsing the line from the log file.
	 */
	public static Pizza createPizza(String line) throws PizzaException, LogHandlerException {
		String[] elements = line.split(",");
		pizzaLineNo++;

		// Check for correct number of elements
		if (elements.length != 9) {
			throw new LogHandlerException("Invalid Format at line " + Integer.toString(pizzaLineNo) + ".");
		}

		// Check no field is empty or contains whitespace before or after the
		// string
		for (int i = 0; i < 9; i++) {
			if (elements[i].length() == 0 || line.trim().length() != line.length()) {
				throw new LogHandlerException("Invalid Format at line " + Integer.toString(pizzaLineNo) + ".");
			}
		}

		// Check valid order time format
		try {
			LocalTime.parse(elements[ORDER_TIME]);
		} catch (Exception e) {
			throw new LogHandlerException("Invalid Order Time at line " + Integer.toString(pizzaLineNo) + ".");
		}

		// Check valid delivery time format
		try {
			LocalTime.parse(elements[DELIVER_TIME]);
		} catch (Exception e) {
			throw new LogHandlerException("Invalid Deliver Time at line " + Integer.toString(pizzaLineNo) + ".");
		}

		// Check valid phone number length and values are only numbers with 0
		// starting
		if (elements[PHONE_NUMBER].length() != 10 || !elements[PHONE_NUMBER].matches("^0[0-9]*$")) {
			throw new LogHandlerException("Invalid Phone Number at line " + Integer.toString(pizzaLineNo) + ".");
		}

		// Check location is a number
		try {
			Integer.parseInt(elements[LOCATION_X]);
			Integer.parseInt(elements[LOCATION_Y]);
		} catch (Exception e) {
			throw new LogHandlerException("Invalid Location at line " + Integer.toString(pizzaLineNo) + ".");
		}

		// Check Quantity is a number
		try {
			Integer.parseInt(elements[QUANTITY]);
		} catch (Exception e) {
			throw new LogHandlerException("Invalid Quantity at line " + Integer.toString(pizzaLineNo) + ".");
		}

		try {
			switch (elements[PIZZA_CODE]) {
			case MARGHERITA:
				return new MargheritaPizza(Integer.parseInt(elements[QUANTITY]), LocalTime.parse(elements[ORDER_TIME]),
						LocalTime.parse(elements[DELIVER_TIME]));
			case VEGETARIAN:
				return new VegetarianPizza(Integer.parseInt(elements[QUANTITY]), LocalTime.parse(elements[ORDER_TIME]),
						LocalTime.parse(elements[DELIVER_TIME]));
			case MEATLOVERS:
				return new MeatLoversPizza(Integer.parseInt(elements[QUANTITY]), LocalTime.parse(elements[ORDER_TIME]),
						LocalTime.parse(elements[DELIVER_TIME]));
			}
		} catch (PizzaException e) {
			throw new PizzaException(e.getMessage() + " at line " + Integer.toString(customerLineNo) + ".");
		}

		// Unable to find pizza code
		throw new PizzaException("Pizza Code invalid at line " + Integer.toString(pizzaLineNo) + ".");
	}

}