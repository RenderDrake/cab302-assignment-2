package asgn2GUIs;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import asgn2Customers.Customer;
import asgn2Pizzas.Pizza;
import asgn2Restaurant.PizzaRestaurant;

public class PizzaGUI extends javax.swing.JFrame implements Runnable, ActionListener {
	private static final long serialVersionUID = -7031008862559936404L;

	private final PizzaRestaurant restaurant;
	private JFileChooser fileChooser;
	private JTable tblCustomer;
	private JTable tblPizza;
	private JTabbedPane tabbedPane;
	private JTextField tfdTotalProfit;
	private JTextField tfdTotalDistance;
	private JButton btnCalculate;
	private JButton btnReset;
	private JButton btnLoad;
	final private int window_width = 1300;
	final private int window_height = 500;

	/**
	 * Create the application.
	 * 
	 * @param title
	 *            - Specifies a string for the title of the GUI
	 */
	public PizzaGUI(String title) throws HeadlessException {
		super(title);
		restaurant = new PizzaRestaurant();
	}

	@Override
	public void run() {
		createGUI();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void createGUI() {
		// Setup JFrame
		setSize(window_width, window_height);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Create tabs
		tabbedPane = new JTabbedPane(SwingConstants.TOP);
		this.getContentPane().add(tabbedPane);
		processTab(tabbedPane);
		orderTab(tabbedPane);

		// Display GUI
		repaint();
		this.setVisible(true);
	}

	/**
	 * Initialise the process tab for loading logs
	 * 
	 * @param tabbedPane
	 *            - Specifies pane to create tab in
	 */
	private void processTab(JTabbedPane tabbedPane) {
		// Create panel for entire tab
		JPanel pnlProcess = new JPanel();

		// Add panel to tab
		tabbedPane.addTab("Totals", null, pnlProcess, null);
		tabbedPane.setEnabledAt(0, true);

		// Create grid layout with 3 columns in the center
		GridBagLayout gbl_pnlProcess = new GridBagLayout();
		gbl_pnlProcess.columnWidths = new int[] { 410, 160, 160, 160, 410 };
		gbl_pnlProcess.rowHeights = new int[] { 30, 30, 30, 30 };
		gbl_pnlProcess.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_pnlProcess.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		pnlProcess.setLayout(gbl_pnlProcess);

		// Add "Total Profit" level to first column aligned to the right
		JLabel lblTotalProfit = new JLabel("Total Profit:");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel.fill = GridBagConstraints.VERTICAL;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 1;
		pnlProcess.add(lblTotalProfit, gbc_lblNewLabel);

		// Add text field for total profit to second column
		tfdTotalProfit = new JTextField();
		tfdTotalProfit.setEditable(false);
		tfdTotalProfit.setMinimumSize(new Dimension(159, 30));
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.anchor = GridBagConstraints.WEST;
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 1;
		pnlProcess.add(tfdTotalProfit, gbc_textField);

		// Add label for "Total Distance" to third column aligned to the right
		JLabel lblTotalDistance = new JLabel("Total Distance:");
		GridBagConstraints gbc_lblTotalDistance = new GridBagConstraints();
		gbc_lblTotalDistance.anchor = GridBagConstraints.EAST;
		gbc_lblTotalDistance.fill = GridBagConstraints.VERTICAL;
		gbc_lblTotalDistance.insets = new Insets(0, 0, 5, 5);
		gbc_lblTotalDistance.gridx = 2;
		gbc_lblTotalDistance.gridy = 1;
		pnlProcess.add(lblTotalDistance, gbc_lblTotalDistance);

		// Add text field for total distance to fourth column
		tfdTotalDistance = new JTextField();
		tfdTotalDistance.setEditable(false);
		tfdTotalDistance.setMinimumSize(new Dimension(159, 30));
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.anchor = GridBagConstraints.WEST;
		gbc_textField_1.insets = new Insets(0, 0, 5, 0);
		gbc_textField_1.gridx = 3;
		gbc_textField_1.gridy = 1;
		pnlProcess.add(tfdTotalDistance, gbc_textField_1);

		// Create load button and add to bottom of screen
		btnLoad = new JButton("Load File");
		btnLoad.addActionListener(this);
		GridBagConstraints gbc_btnLoad = new GridBagConstraints();
		gbc_btnLoad.anchor = GridBagConstraints.EAST;
		gbc_btnLoad.insets = new Insets(0, 0, 5, 0);
		gbc_btnLoad.gridx = 1;
		gbc_btnLoad.gridy = 2;
		pnlProcess.add(btnLoad, gbc_btnLoad);

		// Create calculate button and add to bottom of screen
		btnCalculate = new JButton("Calculate");
		btnCalculate.setEnabled(false);
		btnCalculate.addActionListener(this);
		GridBagConstraints gbc_btnCalculate = new GridBagConstraints();
		gbc_btnCalculate.anchor = GridBagConstraints.CENTER;
		gbc_btnCalculate.insets = new Insets(0, 0, 5, 0);
		gbc_btnCalculate.gridx = 2;
		gbc_btnCalculate.gridy = 2;
		pnlProcess.add(btnCalculate, gbc_btnCalculate);

		// Create reset button and add to bottom of screen
		btnReset = new JButton("Reset");
		btnReset.setEnabled(false);
		btnReset.addActionListener(this);
		GridBagConstraints gbc_btnReset = new GridBagConstraints();
		gbc_btnReset.anchor = GridBagConstraints.WEST;
		gbc_btnReset.insets = new Insets(0, 0, 5, 0);
		gbc_btnReset.gridx = 3;
		gbc_btnReset.gridy = 2;
		pnlProcess.add(btnReset, gbc_btnReset);
	}

	/**
	 * Initialise the orders tab where customers and pizzas are displayed
	 * 
	 * @param tabbedPane
	 *            - Specifies pane to create tab in
	 */
	private void orderTab(JTabbedPane tabbedPane) {
		// Create file chooser and panel for tab
		fileChooser = new JFileChooser();
		JPanel pnlOrder = new JPanel();

		// Add panel to tab
		tabbedPane.addTab("Orders", null, pnlOrder, null);
		tabbedPane.setEnabledAt(1, false);

		// Create grid layout with 3 rows
		GridBagLayout gbl_pnlOrder = new GridBagLayout();
		gbl_pnlOrder.columnWidths = new int[] { this.getWidth() - 25, 0 };
		gbl_pnlOrder.rowHeights = new int[] { 50, this.getHeight() - 150, 50, 0 };
		gbl_pnlOrder.columnWeights = new double[] { 0.0, Double.MIN_VALUE };
		gbl_pnlOrder.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		pnlOrder.setLayout(gbl_pnlOrder);

		// Create header panel at top of panel
		JPanel pnlHeader = new JPanel();
		GridBagConstraints gbc_pnlHeader = new GridBagConstraints();
		gbc_pnlHeader.fill = GridBagConstraints.BOTH;
		gbc_pnlHeader.insets = new Insets(0, 0, 5, 0);
		gbc_pnlHeader.gridx = 0;
		gbc_pnlHeader.gridy = 0;
		pnlOrder.add(pnlHeader, gbc_pnlHeader);
		pnlHeader.setLayout(new GridLayout(1, 2, 0, 0));

		// Add titles to tables
		JLabel lblCustomer = new JLabel("Customer");
		lblCustomer.setHorizontalAlignment(SwingConstants.CENTER);
		pnlHeader.add(lblCustomer);

		JLabel lblPizza = new JLabel("Pizza");
		lblPizza.setHorizontalAlignment(SwingConstants.CENTER);
		pnlHeader.add(lblPizza);

		// Create table panel in center of panel
		JPanel pnlTable = new JPanel();
		GridBagConstraints gbc_pnlTable = new GridBagConstraints();
		gbc_pnlTable.fill = GridBagConstraints.BOTH;
		gbc_pnlTable.insets = new Insets(0, 0, 5, 0);
		gbc_pnlTable.gridx = 0;
		gbc_pnlTable.gridy = 1;
		pnlOrder.add(pnlTable, gbc_pnlTable);
		pnlTable.setLayout(new GridLayout(1, 2, 10, 10));

		// Create customer table with headers
		Object[] customerHeader = { "Name", "Mobile", "Type", "X Location", "Y Location", "Distance" };
		DefaultTableModel customerModel = new DefaultTableModel(customerHeader, 0);

		tblCustomer = new JTable(customerModel);
		tblCustomer.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		pnlTable.add(new JScrollPane(tblCustomer));

		// Create pizza table with headers
		Object[] pizzaHeader = { "Type", "Quantity", "Price", "Cost", "Profit" };
		DefaultTableModel pizzaModel = new DefaultTableModel(pizzaHeader, 0);

		tblPizza = new JTable(pizzaModel);
		tblPizza.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		pnlTable.add(new JScrollPane(tblPizza));
	}

	/**
	 * Performs events on the GUI when a button is pressed
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		Object src = e.getSource();

		// Determine which button is pressed
		if (src == btnLoad) {
			// Load button has been pressed

			final int FIRST_ORDER = 0;
			final String TWO_DECIMAL = "%.2f";

			// Open file explorer and wait for file to be opened
			int returnVal = fileChooser.showOpenDialog(this);

			// If file is opened
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();

				try {
					// Process file and update button and tab availability
					restaurant.processLog(file.getAbsolutePath());
					btnLoad.setEnabled(false);
					tabbedPane.setEnabledAt(1, true);
					btnReset.setEnabled(true);
					btnCalculate.setEnabled(true);

					// Get table contents
					DefaultTableModel customerModel = (DefaultTableModel) tblCustomer.getModel();
					DefaultTableModel pizzaModel = (DefaultTableModel) tblPizza.getModel();

					// Update table contents with data from pizza restaurant
					// (all doubles rounded to 2 decimals)
					for (int orderNum = FIRST_ORDER; orderNum < restaurant.getNumCustomerOrders(); orderNum++) {
						Customer customer = restaurant.getCustomerByIndex(orderNum);
						customerModel.addRow(new Object[] { customer.getName(), customer.getMobileNumber(),
								customer.getCustomerType(), customer.getLocationX(), customer.getLocationY(),
								String.format(TWO_DECIMAL, customer.getDeliveryDistance()) });

						Pizza pizza = restaurant.getPizzaByIndex(orderNum);
						pizzaModel.addRow(new Object[] { pizza.getPizzaType(), pizza.getQuantity(),
								String.format(TWO_DECIMAL, pizza.getOrderPrice()),
								String.format(TWO_DECIMAL, pizza.getOrderCost()),
								String.format(TWO_DECIMAL, pizza.getOrderProfit()) });
					}
				} catch (Exception ex) {
					// Display Exception Message and reset gui display
					JOptionPane.showMessageDialog(this, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					restaurant.resetDetails();
				}
			}
		} else if (src == btnReset) {
			// Reset button pressed

			// Reset details and update button and tab availability
			tabbedPane.setEnabledAt(1, false);
			btnReset.setEnabled(false);
			btnCalculate.setEnabled(false);
			btnLoad.setEnabled(true);
			restaurant.resetDetails();

			// Remove data from tables
			DefaultTableModel customerModel = (DefaultTableModel) tblCustomer.getModel();
			DefaultTableModel pizzaModel = (DefaultTableModel) tblPizza.getModel();

			final int FIRST_ROW = 0;
			final int NUMBER_OF_ROWS = customerModel.getRowCount() - 1;

			for (int rowNum = NUMBER_OF_ROWS; rowNum >= FIRST_ROW; rowNum--) {
				customerModel.removeRow(rowNum);
				pizzaModel.removeRow(rowNum);
			}

			// Remove pricing from TotalDistance and TotalProfit
			tfdTotalDistance.setText("");
			tfdTotalProfit.setText("");
		} else if (src == btnCalculate) {
			// Calculate button pressed

			final String TWO_DECIMAL = "%.2f";

			// Update TotalDistance and TotalProfit Fields
			tfdTotalDistance.setText(String.format(TWO_DECIMAL, restaurant.getTotalDeliveryDistance()));
			tfdTotalProfit.setText(String.format(TWO_DECIMAL, restaurant.getTotalProfit()));
		}
	}
}
