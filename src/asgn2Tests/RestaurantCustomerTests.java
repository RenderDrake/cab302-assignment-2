package asgn2Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import asgn2Customers.DriverDeliveryCustomer;
import asgn2Customers.DroneDeliveryCustomer;
import asgn2Customers.PickUpCustomer;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Restaurant.PizzaRestaurant;

/**
 * A class that that tests the methods relating to the handling of Customer
 * objects in the asgn2Restaurant.PizzaRestaurant class as well as processLog
 * and resetDetails.
 * 
 * @author Sylvester Soetianto
 */
public class RestaurantCustomerTests {
	private File file;
	private static PizzaRestaurant rest;
	private int outOfBounds;
	private static int index;
	private static int numCustomers;
	private static int locationX;
	private static int locationY;
	private static double totalDeliveryDistance;
	private final double acceptableDifferenceBetweenDistances = 0.001;
	private static String name;
	private static String mobileNumber;
	private static String pickType;
	private static String driverType;
	private static String droneType;
	private static DriverDeliveryCustomer driver;
	private static PickUpCustomer pickUp;
	private static DroneDeliveryCustomer drone;

	/**
	 * Get customer specific variables which could change in the future
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */
	@BeforeClass
	static public void setVariables() throws CustomerException {

		name = "Casey Jones";
		mobileNumber = "0123456789";
		locationX = 5;
		locationY = 5;

		driver = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);

		pickUp = new PickUpCustomer(name, mobileNumber, 0, 0);

		name = "Sophia Brown";
		mobileNumber = "0101102333";
		locationX = -2;
		locationY = 4;

		drone = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);

		totalDeliveryDistance = pickUp.getDeliveryDistance() + driver.getDeliveryDistance()
				+ drone.getDeliveryDistance();

		pickType = pickUp.getCustomerType();
		driverType = driver.getCustomerType();
		droneType = drone.getCustomerType();
	}

	/**
	 * Create a new pizza restaurant for the test
	 */
	@Before
	public void newPizzaRestaurant() {
		rest = new PizzaRestaurant();
	}

	/*
	 * Exception Testing
	 */

	/**
	 * Test getCustomerByIndex throws CustomerException when no pizzas have been
	 * ordered
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = CustomerException.class)
	public void noPizzaGetCustomerByIndex() throws CustomerException {
		index = 0;

		rest.getCustomerByIndex(index);
	}

	/**
	 * Test getCustomerByIndex throws CustomerException when index is Out Of
	 * Bounds
	 *
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = CustomerException.class)
	public void indexOOBGetCustomerByIndex() throws PizzaException, CustomerException, LogHandlerException {
		outOfBounds = 1;
		file = new File("logs\\tests\\OneOrder.txt");
		rest.processLog(file.getAbsolutePath());
		rest.getCustomerByIndex(outOfBounds);
	}

	/**
	 * Test resetDetails clears the number of customers
	 *
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = CustomerException.class)
	public void getIndexResetDetailsTest() throws CustomerException, PizzaException, LogHandlerException {
		index = 0;
		file = new File("logs\\tests\\MultipleOrder.txt");
		rest.processLog(file.getAbsolutePath());
		rest.resetDetails();

		rest.getCustomerByIndex(index);
	}

	/**
	 * Test how getProcessLog handles CustomerExceptions
	 *
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = LogHandlerException.class)
	public void customerExceptionProcessLog() throws CustomerException, PizzaException, LogHandlerException {
		file = new File("logs\\tests\\BadPhoneNumber.txt");
		rest.processLog(file.getAbsolutePath());
	}

	/**
	 * Test how getProcessLog handles logHandlerExceptions
	 *
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = LogHandlerException.class)
	public void logHandlerExceptionProcessLog() throws CustomerException, PizzaException, LogHandlerException {
		file = new File("logs\\tests\\BadFormat.txt");
		rest.processLog(file.getAbsolutePath());
	}

	/*
	 * Non Exception Testing
	 */

	/**
	 * Test getNumCustomerOrders works when zero pizzas are ordered, this value
	 * should be the same as getNumCustomerOrders
	 */
	@Test
	public void zeroGetNumCustomers() {
		numCustomers = 0;

		assertEquals(numCustomers, rest.getNumCustomerOrders());
		assertEquals(rest.getNumCustomerOrders(), rest.getNumPizzaOrders());
	}

	/**
	 * Test getNumCustomerOrders works when one pizza has been ordered, this
	 * value should be the same as getNumCustomerOrders
	 *
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneGetNumCustomerOrders() throws CustomerException, PizzaException, LogHandlerException {
		numCustomers = 1;
		file = new File("logs\\tests\\OneOrder.txt");
		rest.processLog(file.getAbsolutePath());

		assertEquals(numCustomers, rest.getNumCustomerOrders());
		assertEquals(rest.getNumCustomerOrders(), rest.getNumPizzaOrders());
	}

	/**
	 * Test getNumCustomerOrders works when multiple pizzas are ordered, this
	 * value should be the same as getNumCustomerOrders
	 *
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleGetNumCustomerOrders() throws CustomerException, PizzaException, LogHandlerException {
		numCustomers = 10;
		file = new File("logs\\tests\\MultipleOrder.txt");
		rest.processLog(file.getAbsolutePath());

		assertEquals(numCustomers, rest.getNumCustomerOrders());
		assertEquals(rest.getNumCustomerOrders(), rest.getNumPizzaOrders());
	}

	/**
	 * Test getCustomerByIndex returns when one pizza has been ordered
	 *
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneCustomerGetCustomerByIndex() throws CustomerException, PizzaException, LogHandlerException {
		index = 0;
		file = new File("logs\\tests\\OneOrder.txt");
		rest.processLog(file.getAbsolutePath());

		// Verify customer is the same
		assertEquals(driver, rest.getCustomerByIndex(index));

	}

	/**
	 * Test getCustomerByIndex returns when multiple pizza have been ordered
	 *
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleCustomerGetCustomerByIndex() throws CustomerException, PizzaException, LogHandlerException {
		index = 4;
		file = new File("logs\\tests\\MultipleOrder.txt");
		rest.processLog(file.getAbsolutePath());

		// Verify customer is the same
		assertEquals(drone, rest.getCustomerByIndex(index));
	}

	/**
	 * Test getTotalDeliveryDistance returns total delivery distance when there
	 * are no customers
	 *
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void zeroGetTotalDistance() throws CustomerException, PizzaException, LogHandlerException {
		totalDeliveryDistance = 0.0;

		assertEquals(totalDeliveryDistance, rest.getTotalDeliveryDistance(), acceptableDifferenceBetweenDistances);
	}

	/**
	 * Test getTotalProfit returns total delivery distance when one order has
	 * been placed
	 *
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneGetTotalDeliveryDistance() throws CustomerException, PizzaException, LogHandlerException {
		totalDeliveryDistance = driver.getDeliveryDistance();
		file = new File("logs\\tests\\OneOrder.txt");
		rest.processLog(file.getAbsolutePath());

		assertEquals(totalDeliveryDistance, rest.getTotalDeliveryDistance(), acceptableDifferenceBetweenDistances);
	}

	/**
	 * Test getTotalDeliveryDistance returns totalDeliveryDistance when multiple
	 * orders have been placed
	 *
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleGeTotalDeliveryDistance() throws CustomerException, PizzaException, LogHandlerException {
		totalDeliveryDistance = driver.getDeliveryDistance() * 5;
		file = new File("logs\\tests\\MultipleDistance.txt");
		rest.processLog(file.getAbsolutePath());

		assertEquals(totalDeliveryDistance, rest.getTotalDeliveryDistance(), acceptableDifferenceBetweenDistances);
	}

	/**
	 * Test resetDetails sets totalDeliveryDistance back to 0
	 *
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void totalDeliveryDistanceIndexResetDetailsTest()
			throws CustomerException, PizzaException, LogHandlerException {
		totalDeliveryDistance = 0.0;
		file = new File("logs\\tests\\MultipleOrder.txt");
		rest.processLog(file.getAbsolutePath());
		rest.resetDetails();

		assertEquals(totalDeliveryDistance, rest.getTotalDeliveryDistance(), acceptableDifferenceBetweenDistances);
	}

	/**
	 * Test resetDetails clears the number of customers
	 *
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void numCustomersResetDetailsTest() throws CustomerException, PizzaException, LogHandlerException {
		numCustomers = 0;
		file = new File("logs\\tests\\MultipleOrder.txt");
		rest.processLog(file.getAbsolutePath());
		rest.resetDetails();

		assertEquals(numCustomers, rest.getNumCustomerOrders());
	}

	/**
	 * Test processLog with one customer
	 *
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneProcessLog() throws CustomerException, PizzaException, LogHandlerException {
		numCustomers = 1;
		index = 0;

		// Process Log Returns true when successfully run
		file = new File("logs\\tests\\OneOrder.txt");
		assertTrue(rest.processLog(file.getAbsolutePath()));

		// Check pizza type and quantity are the same
		assertEquals(numCustomers, rest.getNumCustomerOrders());
		assertEquals(driver, rest.getCustomerByIndex(index));
	}

	/**
	 * Test processLog with multiple pizza
	 *
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleProcessLog() throws CustomerException, PizzaException, LogHandlerException {
		String names[] = { "Emma Brown", "Lucas Anderson", "Sophia Singh", "Bella Chen", "Sophia Brown", "Eli Wang",
				"Riley Brown", "Emma Chen", "Jackson Taylor", "Caden Kumar" };

		String phones[] = { "0602547760", "0755201141", "0193102468", "0265045495", "0101102333", "0858312357",
				"0708426008", "0678585543", "0698979160", "0862001010", };

		String types[] = { driverType, droneType, droneType, pickType, droneType, pickType, droneType, droneType,
				driverType, pickType };

		int locations_X[] = { -1, -4, 1, 0, -2, 0, -2, -4, -5, 0 };
		int locations_Y[] = { 0, 5, 8, 0, 4, 0, 0, 2, -10, 0 };

		numCustomers = 10;
		file = new File("logs\\tests\\MultipleOrder.txt");
		rest.processLog(file.getAbsolutePath());

		// Check customers are the same
		assertEquals(numCustomers, rest.getNumCustomerOrders());

		for (int i = 0; i < names.length; i++) {
			assertEquals(names[i], rest.getCustomerByIndex(i).getName());
			assertEquals(phones[i], rest.getCustomerByIndex(i).getMobileNumber());
			assertEquals(types[i], rest.getCustomerByIndex(i).getCustomerType());
			assertEquals(locations_X[i], rest.getCustomerByIndex(i).getLocationX());
			assertEquals(locations_Y[i], rest.getCustomerByIndex(i).getLocationY());
		}
	}
}
