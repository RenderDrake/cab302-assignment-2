package asgn2Tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import asgn2Customers.DriverDeliveryCustomer;
import asgn2Customers.DroneDeliveryCustomer;
import asgn2Customers.PickUpCustomer;
import asgn2Exceptions.CustomerException;

/**
 * A class that tests the that tests the asgn2Customers.PickUpCustomer,
 * asgn2Customers.DriverDeliveryCustomer, asgn2Customers.DroneDeliveryCustomer
 * classes. Note that an instance of asgn2Customers.DriverDeliveryCustomer
 * should be used to test the functionality of the asgn2Customers.Customer
 * abstract class.
 * 
 * @author Sylvester Soetianto
 * 
 *
 */
public class CustomerTests {
	private DriverDeliveryCustomer driverA, driverD;
	private DroneDeliveryCustomer droneB, droneE;
	private PickUpCustomer pickC, pickF;
	private final String driverType = "Driver Delivery", droneType = "Drone Delivery", pickType = "Pick Up",
			customerName = "Dummy", nothing = "", whiteSpaces = "                    ",
			excessName = "StuffStuffStuffStuffA", customerMobile = "0123456789", wrongMobile = "1123456789",
			alphaMobile = "abcdefghjk", whiteMobile = "          ", excessMobile = "01234567890",
			shortMobile = "012345678";
	private final Integer x = 1, zero_x = 0, excess_x = 11, neg_excess_x = -11, puc_excess_x = -1, y = 1, zero_y = 0,
			excess_y = 11, neg_excess_y = -11, puc_excess_y = -1, distance = 2;
	private String name = customerName, mobileNumber = customerMobile;
	private Integer locationX = x, locationY = y;

	/**
	 * Tests creation of DriverDeliveryCustomer class.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testDriverDeliveryCustomer() throws CustomerException {
		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DroneDeliveryCustomer class.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testDroneDeliveryCustomer() throws CustomerException {
		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of PickUpCustomer class.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testPickUpCustomer() throws CustomerException {
		locationX = zero_x;
		locationY = zero_y;

		pickC = new PickUpCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DriverDeliveryCustomer class with an invalid
	 * customerName.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDriverDeliveryCustomerNoName() throws CustomerException {
		name = nothing;

		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DroneDeliveryCustomer class with an invalid
	 * customerName.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDroneDeliveryCustomerNoName() throws CustomerException {
		name = nothing;

		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of PickUpCustomer class with an invalid customerName.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testPickUpCustomerNoName() throws CustomerException {
		locationX = zero_x;
		locationY = zero_y;
		name = nothing;

		pickC = new PickUpCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DriverDeliveryCustomer class with an invalid
	 * customerName.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDriverDeliveryCustomerWhiteSpacesName() throws CustomerException {
		name = whiteSpaces;

		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DroneDeliveryCustomer class with an invalid
	 * customerName.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDroneDeliveryCustomerWhiteSpacesName() throws CustomerException {
		name = whiteSpaces;

		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of PickUpCustomer class with an invalid customerName.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testPickUpCustomerWhiteSpacesName() throws CustomerException {
		locationX = zero_x;
		locationY = zero_y;
		name = whiteSpaces;

		pickC = new PickUpCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DriverDeliveryCustomer class with an invalid
	 * customerName length.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDriverDeliveryCustomerNameExcessLength() throws CustomerException {
		name = excessName;

		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DroneDeliveryCustomer class with an invalid
	 * customerName length.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDroneDeliveryCustomerNameExcessLength() throws CustomerException {
		name = excessName;

		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DriverDeliveryCustomer class with an invalid
	 * customerName length.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testPickUpCustomerNameExcessLength() throws CustomerException {
		locationX = zero_x;
		locationY = zero_y;
		name = excessName;

		pickC = new PickUpCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DriverDeliveryCustomer class with an invalid
	 * customerMobile.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDriverDeliveryCustomerNoMobile() throws CustomerException {
		mobileNumber = nothing;

		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DroneDeliveryCustomer class with an invalid
	 * customerMobile.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDroneDeliveryCustomerNoMobile() throws CustomerException {
		mobileNumber = nothing;

		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of PickUpCustomer class with an invalid customerMobile.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testPickUpCustomerNoMobile() throws CustomerException {
		locationX = zero_x;
		locationY = zero_y;
		mobileNumber = nothing;

		pickC = new PickUpCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DriverDeliveryCustomer class with a whitespace
	 * customerMobile.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDriverDeliveryCustomerWhiteSpacesMobile() throws CustomerException {
		mobileNumber = whiteMobile;

		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DroneDeliveryCustomer class with a whitespace
	 * customerMobile.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDroneDeliveryCustomerWhiteSpacesMobile() throws CustomerException {
		mobileNumber = whiteMobile;

		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of PickUpCustomer class with a whitespace customerMobile.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testPickUpCustomerWhiteSpacesMobile() throws CustomerException {
		locationX = zero_x;
		locationY = zero_y;
		mobileNumber = whiteMobile;

		pickC = new PickUpCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DriverDeliveryCustomer class with a customerMobile with
	 * no digits.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDriverDeliveryCustomerNoDigitsMobile() throws CustomerException {
		mobileNumber = alphaMobile;

		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DroneDeliveryCustomer class with a customerMobile with
	 * no digits.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDroneDeliveryCustomerNoDigitsMobile() throws CustomerException {
		mobileNumber = alphaMobile;

		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of PickUpCustomer class with a customerMobile with no
	 * digits.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testPickUpCustomerNoDigitsMobile() throws CustomerException {
		locationX = zero_x;
		locationY = zero_y;
		mobileNumber = alphaMobile;

		pickC = new PickUpCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DriverDeliveryCustomer class with a customerMobile of
	 * incorrect digit length.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDriverDeliveryCustomerInsufficientMobile() throws CustomerException {
		mobileNumber = shortMobile;

		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DroneDeliveryCustomer class with a customerMobile
	 * incorrect digit length.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDroneDeliveryCustomerInsufficientMobile() throws CustomerException {
		mobileNumber = shortMobile;

		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of PickUpCustomer class with a customerMobile incorrect
	 * digit length.
	 * 
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testPickUpCustomerInsufficientMobile() throws CustomerException {
		locationX = zero_x;
		locationY = zero_y;
		mobileNumber = shortMobile;

		pickC = new PickUpCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DriverDeliveryCustomer class with an invalid
	 * customerMobile starting number.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDriverDeliveryCustomerMobileInvalidStart() throws CustomerException {
		mobileNumber = wrongMobile;

		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DroneDeliveryCustomer class with an invalid
	 * customerMobile starting number.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDroneDeliveryCustomerMobileInvalidStart() throws CustomerException {
		mobileNumber = wrongMobile;

		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of PickUpCustomer class with an invalid customerMobile
	 * starting number.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testPickUpCustomerMobileInvalidStart() throws CustomerException {
		locationX = zero_x;
		locationY = zero_y;
		mobileNumber = wrongMobile;

		pickC = new PickUpCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DriverDeliveryCustomer class with an invalid
	 * customerMobile length.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDriverDeliveryCustomerMobileInvalidLength() throws CustomerException {
		mobileNumber = excessMobile;

		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DroneDeliveryCustomer class with an invalid
	 * customerMobile length.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDroneDeliveryCustomerMobileInvalidLength() throws CustomerException {
		mobileNumber = excessMobile;

		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of PickUpCustomer class with an invalid customerMobile
	 * length.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testPickUpCustomerMobileInvalidLength() throws CustomerException {
		locationX = zero_x;
		locationY = zero_y;
		mobileNumber = excessMobile;

		pickC = new PickUpCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DriverDeliveryCustomer class with an excess negative x
	 * coordinate.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDriverDeliveryCustomerNegExcessX() throws CustomerException {
		locationX = neg_excess_x;

		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DroneDeliveryCustomer class with an excess negative x
	 * coordinate.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDroneDeliveryCustomerNegExcessX() throws CustomerException {
		locationX = neg_excess_x;

		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of PickUpCustomer class with an excess negative x
	 * coordinate.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testPickUpCustomerNegativeX() throws CustomerException {
		locationX = puc_excess_x;
		locationY = zero_y;

		pickC = new PickUpCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DriverDeliveryCustomer class with an excess x
	 * coordinate.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDriverDeliveryCustomerExcessX() throws CustomerException {
		locationX = excess_x;

		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DroneDeliveryCustomer class with an excess x
	 * coordinate.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDroneDeliveryCustomerExcessX() throws CustomerException {
		locationX = excess_x;

		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of PickUpCustomer class with an excess x coordinate.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testPickUpCustomerExcessX() throws CustomerException {
		locationX = x;
		locationY = zero_y;

		pickC = new PickUpCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DriverDeliveryCustomer class with an excess negative y
	 * coordinate.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDriverDeliveryCustomerNegExcessY() throws CustomerException {
		locationY = neg_excess_y;

		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DroneDeliveryCustomer class with an excess negative y
	 * coordinate.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDroneDeliveryCustomerNegExcessY() throws CustomerException {
		locationY = neg_excess_y;

		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of PickUpCustomer class with an excess negative y
	 * coordinate.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testPickUpCustomerNegExcessY() throws CustomerException {
		locationX = zero_x;
		locationY = puc_excess_y;

		pickC = new PickUpCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DriverDeliveryCustomer class with an excess y
	 * coordinate.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDriverDeliveryCustomerExcessY() throws CustomerException {
		locationY = excess_y;

		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DroneDeliveryCustomer class with an excess y
	 * coordinate.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDroneDeliveryCustomerExcessY() throws CustomerException {
		locationY = excess_y;

		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of PickUpCustomer class with an excess y coordinate.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testPickUpCustomerExcessY() throws CustomerException {
		locationX = zero_x;
		locationY = y;

		pickC = new PickUpCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DriverDeliveryCustomer with no delivery distance.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDriverDeliveryCustomerNoDistance() throws CustomerException {
		locationX = zero_x;
		locationY = zero_y;

		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DroneDeliveryCustomer with no delivery distance.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDroneDeliveryCustomerNoDistance() throws CustomerException {
		locationX = zero_x;
		locationY = zero_y;

		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DriverDeliveryCustomer with negative excess delivery
	 * distance.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDriverDeliveryCustomerNegExcessDeliveryDistance() throws CustomerException {
		locationX = neg_excess_x;
		locationY = neg_excess_y;

		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DroneDeliveryCustomer with negative excess delivery
	 * distance.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDroneDeliveryCustomerNegExcessDeliveryDistance() throws CustomerException {
		locationX = neg_excess_x;
		locationY = neg_excess_y;

		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DriverDeliveryCustomer with no delivery distance.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testPickUpCustomerNegExcessDistance() throws CustomerException {
		locationX = puc_excess_x;
		locationY = puc_excess_y;

		pickC = new PickUpCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DriverDeliveryCustomer with an excess delivery
	 * distance.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDriverDeliveryCustomerExcessDeliveryDistance() throws CustomerException {
		locationX = excess_x;
		locationY = excess_y;

		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of DroneDeliveryCustomer with an excess delivery distance.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testDroneDeliveryCustomerExcessDeliveryDistance() throws CustomerException {
		locationX = excess_x;
		locationY = excess_y;

		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests creation of PickUpCustomer with an excess delivery distance.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testPickUpCustomerExcessDistance() throws CustomerException {
		locationX = x;
		locationY = y;

		pickC = new PickUpCustomer(name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests retrieval of a DriverDeliveryCustomer's delivery distance.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testDeliveryCustomerGetDeliveryDistance() throws CustomerException {
		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
		assertTrue(driverA.getDeliveryDistance() == distance);
	}

	/**
	 * Tests retrieval of a DroneDeliveryCustomer's delivery distance.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testDroneDeliveryCustomerGetDeliveryDistance() throws CustomerException {
		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);

		assertTrue(droneB.getDeliveryDistance() == Math
				.sqrt(Math.pow(droneB.getLocationX(), 2) + Math.pow(droneB.getLocationY(), 2)));
	}

	/**
	 * Tests retrieval of a PickUpCustomer's delivery distance.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testPickUpCustomerGetDeliveryDistance() throws CustomerException {
		locationX = zero_x;
		locationY = zero_y;

		pickC = new PickUpCustomer(name, mobileNumber, locationX, locationY);
		assertTrue(pickC.getDeliveryDistance() == locationX);
	}

	/**
	 * Tests retrieval of a DriverDeliveryCustomer's customerName.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testDriverDeliveryCustomerGetName() throws CustomerException {
		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
		assertTrue(driverA.getName().equals(name));
	}

	/**
	 * Tests retrieval of a DroneDeliveryCustomer's customerName.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testDroneDeliveryCustomerGetName() throws CustomerException {
		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);
		assertTrue(droneB.getName().equals(name));
	}

	/**
	 * Tests retrieval of a PickUpCustomer's customerName.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testPickUpCustomerGetName() throws CustomerException {
		locationX = zero_x;
		locationY = zero_y;

		pickC = new PickUpCustomer(name, mobileNumber, locationX, locationY);
		assertTrue(pickC.getName().equals(name));
	}

	/**
	 * Tests retrieval of a DriverDeliveryCustomer's customerMobile.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testDriverDeliveryCustomerGetMobileNumber() throws CustomerException {
		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
		assertTrue(driverA.getMobileNumber().equals(mobileNumber));
	}

	/**
	 * Tests retrieval of a DroneDeliveryCustomer's customerMobile.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testDroneDeliveryCustomerGetMobileNumber() throws CustomerException {
		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);
		assertTrue(droneB.getMobileNumber().equals(mobileNumber));
	}

	/**
	 * Tests retrieval of a PickUpCustomer's customerMobile.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testPickUpCustomerGetMobileNumber() throws CustomerException {
		locationX = zero_x;
		locationY = zero_y;

		pickC = new PickUpCustomer(name, mobileNumber, locationX, locationY);
		assertTrue(pickC.getMobileNumber().equals(mobileNumber));
	}

	/**
	 * Tests retrieval of a DriverDeliveryCustomer's customer type.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testDriverDeliveryCustomerGetCustomerType() throws CustomerException {
		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
		assertTrue(driverA.getCustomerType().equals(driverType));
	}

	/**
	 * Tests retrieval of a DroneDeliveryCustomer's customer type.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testDroneDeliveryCustomerGetCustomerType() throws CustomerException {
		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);
		assertTrue(droneB.getCustomerType().equals(droneType));
	}

	/**
	 * Tests retrieval of a PickUpCustomer's customer type.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testPickUpCustomerGetCustomerType() throws CustomerException {
		locationX = zero_x;
		locationY = zero_y;

		pickC = new PickUpCustomer(name, mobileNumber, locationX, locationY);
		assertTrue(pickC.getCustomerType().equals(pickType));
	}

	/**
	 * Tests retrieval of a DriverDeliveryCustomer's x coordinate.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testDriverDeliveryCustomerGetLocationX() throws CustomerException {
		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
		assertTrue(driverA.getLocationX() == locationX);
	}

	/**
	 * Tests retrieval of a DroneDeliveryCustomer's x coordinate.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testDroneDeliveryCustomerGetLocationX() throws CustomerException {
		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);
		assertTrue(droneB.getLocationX() == locationX);
	}

	/**
	 * Tests retrieval of a PickUpCustomer's x coordinate.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testPickUpCustomerGetLocationX() throws CustomerException {
		locationX = zero_x;
		locationY = zero_y;

		pickC = new PickUpCustomer(name, mobileNumber, locationX, locationY);
		assertTrue(pickC.getLocationX() == locationX);
	}

	/**
	 * Tests retrieval of a DriverDeliveryCustomer's y coordinate.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testDriverDeliveryCustomerGetLocationY() throws CustomerException {
		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
		assertTrue(driverA.getLocationY() == locationY);
	}

	/**
	 * Tests retrieval of a DroneDeliveryCustomer's y coordinate.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testDroneDeliveryCustomerGetLocationY() throws CustomerException {
		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);
		assertTrue(droneB.getLocationY() == locationY);
	}

	/**
	 * Tests retrieval of a PickUpCustomer's y coordinate.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testPickUpCustomerGetLocationY() throws CustomerException {
		locationX = zero_x;
		locationY = zero_y;

		pickC = new PickUpCustomer(name, mobileNumber, locationX, locationY);
		assertTrue(pickC.getLocationY() == locationY);
	}

	/**
	 * Tests comparison of two DriverDeliveryCustomer objects.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testDriverDeliveryCustomerEquals() throws CustomerException {
		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
		driverD = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
		assertTrue(driverA.equals(driverD) && driverD.equals(driverA));
	}

	/**
	 * Tests comparison of two DroneDeliveryCustomer objects.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testDroneDeliveryCustomerEquals() throws CustomerException {
		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);
		droneE = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);
		assertTrue(droneB.equals(droneE) && droneE.equals(droneB));
	}

	/**
	 * Tests comparison of two PickUpyCustomer objects.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testPickUpCustomerEquals() throws CustomerException {
		locationX = zero_x;
		locationY = zero_y;

		pickC = new PickUpCustomer(name, mobileNumber, locationX, locationY);
		pickF = new PickUpCustomer(name, mobileNumber, locationX, locationY);
		assertTrue(pickC.equals(pickF) && pickF.equals(pickC));
	}

	/**
	 * Tests comparison between DriverDeliveryCustomer and DroneDeliveryCustomer
	 * objects.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testDriverDeliveryCustomerDifference() throws CustomerException {
		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);
		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);
		assertFalse(driverA.equals(droneB));
		assertFalse(droneB.equals(driverA));
	}

	/**
	 * Tests comparison between PickUpCustomer and DroneDeliveryCustomer
	 * objects.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testDroneDeliveryCustomerDifference() throws CustomerException {
		droneB = new DroneDeliveryCustomer(name, mobileNumber, locationX, locationY);

		locationX = zero_x;
		locationY = zero_y;

		pickC = new PickUpCustomer(name, mobileNumber, locationX, locationY);
		assertFalse(droneB.equals(pickC));
		assertFalse(pickC.equals(droneB));
	}

	/**
	 * Tests comparison between DriverDeliveryCustomer and PickUpCustomer
	 * objects.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testPickUpCustomerDifference() throws CustomerException {
		driverA = new DriverDeliveryCustomer(name, mobileNumber, locationX, locationY);

		locationX = zero_x;
		locationY = zero_y;

		pickC = new PickUpCustomer(name, mobileNumber, locationX, locationY);
		assertFalse(driverA.equals(pickC));
		assertFalse(pickC.equals(driverA));

	}
}
