package asgn2Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.MargheritaPizza;
import asgn2Pizzas.MeatLoversPizza;
import asgn2Pizzas.VegetarianPizza;
import asgn2Restaurant.PizzaRestaurant;

/**
 * A class that tests the methods relating to the handling of Pizza objects in
 * the asgn2Restaurant.PizzaRestaurant class as well as processLog and
 * resetDetails.
 * 
 * @author Charles Alleman
 *
 */
public class RestaurantPizzaTests {
	private File file;
	private static PizzaRestaurant rest;
	private int outOfBounds;
	private static int index;
	private static int numPizzas;
	private static int numOrders;
	private static LocalTime orderTime;
	private static LocalTime deliverTime;
	private final double acceptableDifferenceBetweenPrices = 0.001;
	private static double lProfit;
	private static double vProfit;
	private static double mProfit;
	private static double profit;
	private static String lType;
	private static String vType;
	private static String mType;

	/**
	 * Get pizza specific variables which could change in the future
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 */
	@BeforeClass
	static public void setVariables() throws PizzaException {
		numPizzas = 1;
		orderTime = LocalTime.of(19, 0);
		deliverTime = LocalTime.of(20, 0);

		MargheritaPizza margherita = new MargheritaPizza(numPizzas, orderTime, deliverTime);
		MeatLoversPizza meatlovers = new MeatLoversPizza(numPizzas, orderTime, deliverTime);
		VegetarianPizza vegetarian = new VegetarianPizza(numPizzas, orderTime, deliverTime);

		mProfit = margherita.getOrderProfit();
		lProfit = meatlovers.getOrderProfit();
		vProfit = vegetarian.getOrderProfit();

		mType = margherita.getPizzaType();
		lType = meatlovers.getPizzaType();
		vType = vegetarian.getPizzaType();
	}

	/**
	 * Create a new pizza restaurant for the test
	 */
	@Before
	public void newPizzaRestaurant() {
		rest = new PizzaRestaurant();
	}

	/*
	 * Exception Testing
	 */

	/**
	 * Test getPizzaByIndex throws PizzaException when no pizzas have been
	 * ordered
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 */
	@Test(expected = PizzaException.class)
	public void noPizzaGetPizzaByIndex() throws PizzaException {
		index = 0;

		rest.getPizzaByIndex(index);
	}

	/**
	 * Test getPizzaByIndex throws PizzaException when index is Out Of Bounds
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = PizzaException.class)
	public void indexOOBGetPizzaByIndex() throws PizzaException, CustomerException, LogHandlerException {
		outOfBounds = 1;
		file = new File("logs\\tests\\OneOrder.txt");
		rest.processLog(file.getAbsolutePath());
		rest.getPizzaByIndex(outOfBounds);
	}

	/**
	 * Test resetDetails clears the number of pizzas
	 * 
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 */
	@Test(expected = PizzaException.class)
	public void getIndexResetDetailsTest() throws CustomerException, PizzaException, LogHandlerException {
		index = 0;
		file = new File("logs\\tests\\MultipleOrder.txt");
		rest.processLog(file.getAbsolutePath());
		rest.resetDetails();

		rest.getPizzaByIndex(index);
	}

	/**
	 * Test how getProcessLog handles pizzaExceptions
	 * 
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 */
	@Test(expected = PizzaException.class)
	public void pizzaExceptionProcessLog() throws CustomerException, PizzaException, LogHandlerException {
		file = new File("logs\\tests\\ExcessQuantity.txt");
		rest.processLog(file.getAbsolutePath());
	}

	/**
	 * Test how getProcessLog handles logHandlerExceptions
	 * 
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 */
	@Test(expected = LogHandlerException.class)
	public void logHandlerExceptionProcessLog() throws CustomerException, PizzaException, LogHandlerException {
		file = new File("logs\\tests\\BadFormat.txt");
		rest.processLog(file.getAbsolutePath());
	}

	/*
	 * Non Exception Testing
	 */

	/**
	 * Test getNumPizzaOrders works when zero pizzas are ordered, this value
	 * should be the same as getNumCustomerOrders
	 */
	@Test
	public void zeroGetNumPizzaOrders() {
		numOrders = 0;

		assertEquals(numOrders, rest.getNumPizzaOrders());
		assertEquals(rest.getNumCustomerOrders(), rest.getNumPizzaOrders());
	}

	/**
	 * Test getNumPizzaOrders works when one pizza has been ordered, this value
	 * should be the same as getNumCustomerOrders
	 * 
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 */
	@Test
	public void oneGetNumPizzaOrders() throws CustomerException, PizzaException, LogHandlerException {
		numOrders = 1;
		file = new File("logs\\tests\\OneOrder.txt");
		rest.processLog(file.getAbsolutePath());

		assertEquals(numOrders, rest.getNumPizzaOrders());
		assertEquals(rest.getNumCustomerOrders(), rest.getNumPizzaOrders());
	}

	/**
	 * Test getNumPizzaOrders works when multiple pizzas are ordered, this value
	 * should be the same as getNumCustomerOrders
	 * 
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 */
	@Test
	public void multipleGetNumPizzaOrders() throws CustomerException, PizzaException, LogHandlerException {
		numOrders = 10;
		file = new File("logs\\tests\\MultipleOrder.txt");
		rest.processLog(file.getAbsolutePath());

		assertEquals(numOrders, rest.getNumPizzaOrders());
		assertEquals(rest.getNumCustomerOrders(), rest.getNumPizzaOrders());
	}

	/**
	 * Test getPizzaByIndex returns when one pizza has been ordered
	 * 
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 */
	@Test
	public void onePizzaGetPizzaByIndex() throws CustomerException, PizzaException, LogHandlerException {
		numPizzas = 1;
		index = 0;
		file = new File("logs\\tests\\OneOrder.txt");
		rest.processLog(file.getAbsolutePath());

		// Verify pizza and quantity are the same
		assertEquals(numPizzas, rest.getPizzaByIndex(index).getQuantity());
		assertEquals(vType, rest.getPizzaByIndex(index).getPizzaType());
	}

	/**
	 * Test getPizzaByIndex returns when multiple pizza have been ordered
	 * 
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 */
	@Test
	public void multuplePizzaGetPizzaByIndex() throws CustomerException, PizzaException, LogHandlerException {
		numPizzas = 7;
		index = 4;
		file = new File("logs\\tests\\MultipleOrder.txt");
		rest.processLog(file.getAbsolutePath());

		// Verify pizza and quantity are the same
		assertEquals(numPizzas, rest.getPizzaByIndex(index).getQuantity());
		assertEquals(vType, rest.getPizzaByIndex(index).getPizzaType());
	}

	/**
	 * Test getPizzaByIndex returns the pizza at index zero
	 * 
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 */
	@Test
	public void zeroGetPizzaByIndex() throws CustomerException, PizzaException, LogHandlerException {
		numPizzas = 5;
		index = 0;
		file = new File("logs\\tests\\MultipleOrder.txt");
		rest.processLog(file.getAbsolutePath());

		// Verify pizza and quantity are the same
		assertEquals(numPizzas, rest.getPizzaByIndex(index).getQuantity());
		assertEquals(vType, rest.getPizzaByIndex(index).getPizzaType());
	}

	/**
	 * Test getPizzaByIndex returns the pizza at a random index
	 * 
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 */
	@Test
	public void middleGetPizzaByIndex() throws CustomerException, PizzaException, LogHandlerException {
		numPizzas = 7;
		index = 5;
		file = new File("logs\\tests\\MultipleOrder.txt");
		rest.processLog(file.getAbsolutePath());

		// Verify pizza and quantity are the same
		assertEquals(numPizzas, rest.getPizzaByIndex(index).getQuantity());
		assertEquals(lType, rest.getPizzaByIndex(index).getPizzaType());
	}

	/**
	 * Test getPizzaByIndex returns the pizza at the last index
	 * 
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 */
	@Test
	public void lastGetPizzaByIndex() throws CustomerException, PizzaException, LogHandlerException {
		numPizzas = 9;
		index = 9;
		file = new File("logs\\tests\\MultipleOrder.txt");
		rest.processLog(file.getAbsolutePath());

		// Verify pizza and quantity are the same
		assertEquals(numPizzas, rest.getPizzaByIndex(index).getQuantity());
		assertEquals(lType, rest.getPizzaByIndex(index).getPizzaType());
	}

	/**
	 * Test getTotalProfit returns profit when no orders have been placed
	 * 
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 */
	@Test
	public void zeroGetTotalProfit() throws CustomerException, PizzaException, LogHandlerException {
		profit = 0.0;

		assertEquals(profit, rest.getTotalProfit(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Test getTotalProfit returns profit when one order has been placed
	 * 
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 */
	@Test
	public void oneGetTotalProfit() throws CustomerException, PizzaException, LogHandlerException {
		profit = vProfit;
		file = new File("logs\\tests\\OneOrder.txt");
		rest.processLog(file.getAbsolutePath());

		assertEquals(profit, rest.getTotalProfit(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Test getTotalProfit returns profit when a pizza order with multiple
	 * pizzas is placed
	 * 
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 */
	@Test
	public void multipleQuantityGetTotalProfit() throws CustomerException, PizzaException, LogHandlerException {
		profit = vProfit * 5;
		file = new File("logs\\tests\\MultipleQuantity.txt");
		rest.processLog(file.getAbsolutePath());

		assertEquals(profit, rest.getTotalProfit(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Test getTotalProfit returns profit when multiple orders have been placed
	 * 
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 */
	@Test
	public void multipleGetTotalProfit() throws CustomerException, PizzaException, LogHandlerException {
		profit = vProfit * 16 + lProfit * 34 + mProfit;
		file = new File("logs\\tests\\MultipleOrder.txt");
		rest.processLog(file.getAbsolutePath());

		assertEquals(profit, rest.getTotalProfit(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Test resetDetails sets profit back to 0
	 * 
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 */
	@Test
	public void profitIndexResetDetailsTest() throws CustomerException, PizzaException, LogHandlerException {
		profit = 0.0;
		file = new File("logs\\tests\\MultipleOrder.txt");
		rest.processLog(file.getAbsolutePath());
		rest.resetDetails();

		assertEquals(profit, rest.getTotalProfit(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Test resetDetails clears the number of pizzas
	 * 
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 */
	@Test
	public void numPizzasResetDetailsTest() throws CustomerException, PizzaException, LogHandlerException {
		numOrders = 0;
		file = new File("logs\\tests\\MultipleOrder.txt");
		rest.processLog(file.getAbsolutePath());
		rest.resetDetails();

		assertEquals(numOrders, rest.getNumPizzaOrders());
	}

	/**
	 * Test processLog with one pizza
	 * 
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 */
	@Test
	public void oneProcessLog() throws CustomerException, PizzaException, LogHandlerException {
		numOrders = 1;
		index = 0;

		// Process Log Returns true when successfully run
		file = new File("logs\\tests\\OneQuantity.txt");
		assertTrue(rest.processLog(file.getAbsolutePath()));

		// Check pizza type and quantity are the same
		assertEquals(numOrders, rest.getNumPizzaOrders());
		assertEquals(vType, rest.getPizzaByIndex(index).getPizzaType());
	}

	/**
	 * Test processLog with a pizza with more than 1 quantity
	 * 
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 */
	@Test
	public void multiplePizzaProcessLog() throws CustomerException, PizzaException, LogHandlerException {
		numOrders = 1;
		index = 0;
		file = new File("logs\\tests\\MultipleQuantity.txt");
		assertTrue(rest.processLog(file.getAbsolutePath()));

		// Check pizza type and quantity are the same
		assertEquals(numOrders, rest.getNumPizzaOrders());
		assertEquals(vType, rest.getPizzaByIndex(index).getPizzaType());
	}

	/**
	 * Test processLog with multiple pizza
	 * 
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 * @throws CustomerException
	 *             if supplied parameters are invalid if supplied parameters are
	 *             invalid
	 */
	@Test
	public void multipleProcessLog() throws CustomerException, PizzaException, LogHandlerException {
		int quantities[] = { 5, 9, 2, 1, 7, 7, 2, 4, 5, 9 };
		String types[] = { vType, lType, vType, mType, vType, lType, vType, lType, lType, lType };
		numOrders = 10;
		file = new File("logs\\tests\\MultipleOrder.txt");
		rest.processLog(file.getAbsolutePath());

		// Check pizza type and quantity are the same
		assertEquals(numOrders, rest.getNumPizzaOrders());

		for (int i = 0; i < quantities.length; i++) {
			assertEquals(quantities[i], rest.getPizzaByIndex(i).getQuantity());
			assertEquals(types[i], rest.getPizzaByIndex(i).getPizzaType());
		}
	}
}
