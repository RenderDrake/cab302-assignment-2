package asgn2Tests;

import java.time.LocalTime;

import org.junit.Test;

import asgn2Exceptions.PizzaException;
import asgn2Pizzas.PizzaFactory;

//TODO Test if the function uses String = String or String.compareTo()

/**
 * A class that tests the asgn2Pizzas.PizzaFactory class.
 * 
 * @author Charles Alleman
 * 
 */
public class PizzaFactoryTests {
	private final String margheritaCode = "PZM";
	private final String vegetarianCode = "PZV";
	private final String meatLoversCode = "PZL";
	private final String customerCode = "DNC";
	private int quantity;
	private LocalTime orderTime;
	private LocalTime deliveryTime;

	/*
	 * Exception Testing
	 */

	/**
	 * Test getPizza function throws pizza exception when empty pizzaCode is
	 * used
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void emptyPizzaCodeGetPizzaTest() throws PizzaException {
		String falseCode = "";
		quantity = 1;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		PizzaFactory.getPizza(falseCode, quantity, orderTime, deliveryTime);
	}

	/**
	 * Test getPizza function throws pizza exception when incorrect pizzaCode is
	 * used with same number of characters as actual code
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void sameSizePizzaCodeGetPizzaTest() throws PizzaException {
		String falseCode = "ABC";
		quantity = 1;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		PizzaFactory.getPizza(falseCode, quantity, orderTime, deliveryTime);
	}

	/**
	 * Test getPizza function throws pizza exception when pizza code is a
	 * customer code
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void customerPizzaCodeGetPizzaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		PizzaFactory.getPizza(customerCode, quantity, orderTime, deliveryTime);
	}

	/**
	 * Test getPizza function throws pizza exception with a lower case code
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void caseSensitivityCodeGetPizzaTest() throws PizzaException {
		String falseCode = margheritaCode.toLowerCase();
		quantity = 1;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		PizzaFactory.getPizza(falseCode, quantity, orderTime, deliveryTime);
	}

	/**
	 * Test getPizza function throws pizza exception when incorrect pizzaCode is
	 * used with same number of characters as actual code
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void invalidPizzaCodeGetPizzaTest() throws PizzaException {
		String falseCode = "KJSHFJSHGD";
		quantity = 1;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		PizzaFactory.getPizza(falseCode, quantity, orderTime, deliveryTime);
	}

	/*
	 * Non-Exception Testing
	 */

	/**
	 * Test margherita can be made
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void margheritaGetPizzaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		PizzaFactory.getPizza(margheritaCode, quantity, orderTime, deliveryTime);
	}

	/**
	 * Test vegetarian can be made
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void vegetarianGetPizzaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		PizzaFactory.getPizza(vegetarianCode, quantity, orderTime, deliveryTime);
	}

	/**
	 * Test meatlovers can be made
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void meatLoversGetPizzaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		PizzaFactory.getPizza(meatLoversCode, quantity, orderTime, deliveryTime);
	}

	/**
	 * Test with one pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneQuantityGetPizzaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		PizzaFactory.getPizza(meatLoversCode, quantity, orderTime, deliveryTime);
	}

	/**
	 * Test with multiple pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleQuantityGetPizzarTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		PizzaFactory.getPizza(meatLoversCode, quantity, orderTime, deliveryTime);
	}

	/**
	 * Test with the maximum number of pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void maxQuantityGetPizzaTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		PizzaFactory.getPizza(meatLoversCode, quantity, orderTime, deliveryTime);
	}

	/**
	 * Test getPizza function works at opening time
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void openOrderGetPizzaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(19, 0); // 7pm
		deliveryTime = LocalTime.of(19, 30); // 7:30pm

		PizzaFactory.getPizza(meatLoversCode, quantity, orderTime, deliveryTime);
	}

	/**
	 * Test getPizza function works with a random time in the working hours
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void workingHoursOrderGetPizzaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(20, 21); // 8:21pm
		deliveryTime = LocalTime.of(20, 44); // 8:44pm

		PizzaFactory.getPizza(meatLoversCode, quantity, orderTime, deliveryTime);
	}

	/**
	 * Test getPizza function works at the closing time
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void closingOrderGetPizzaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(22, 59); // 10:59pm
		deliveryTime = LocalTime.of(23, 30); // 11:30pm

		PizzaFactory.getPizza(meatLoversCode, quantity, orderTime, deliveryTime);
	}

	/**
	 * Test getPizza function works where the delivery time is 10 minutes after
	 * order
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void fastestOrderGetPizzaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(19, 0); // 7:00pm
		deliveryTime = LocalTime.of(19, 10); // 7:10pm

		PizzaFactory.getPizza(meatLoversCode, quantity, orderTime, deliveryTime);
	}

	/**
	 * Test getPizza function works where the delivery time is 1 hour and 9
	 * minutes
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void longestOrderGetPizzaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(19, 0); // 7:00pm
		deliveryTime = LocalTime.of(20, 8); // 8:09pm

		PizzaFactory.getPizza(meatLoversCode, quantity, orderTime, deliveryTime);
	}

	/**
	 * Test getPizza function works for the latest possible order time possible
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void latestOrderGetPizzaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(22, 59); // 10:59pm
		deliveryTime = LocalTime.of(0, 8); // 12:08pm

		PizzaFactory.getPizza(meatLoversCode, quantity, orderTime, deliveryTime);
	}

	/*
	 * Test that exceptions are handled by the getPizza function
	 */

	/**
	 * Test negative number of pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void negativeQuantityGetPizzaTest() throws PizzaException {
		quantity = -1;
		orderTime = LocalTime.of(20, 21); // 8:21pm
		deliveryTime = LocalTime.of(20, 44); // 8:44pm

		PizzaFactory.getPizza(meatLoversCode, quantity, orderTime, deliveryTime);
	}

	/**
	 * Test zero pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void zeroQuantityGetPizzaTest() throws PizzaException {
		quantity = 0;
		orderTime = LocalTime.of(20, 21); // 8:21pm
		deliveryTime = LocalTime.of(20, 44); // 8:44pm

		PizzaFactory.getPizza(meatLoversCode, quantity, orderTime, deliveryTime);
	}

	/**
	 * Test more than 10 pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void excessQuantityGetPizzaTest() throws PizzaException {
		quantity = 11;
		orderTime = LocalTime.of(20, 21); // 8:21pm
		deliveryTime = LocalTime.of(20, 44); // 8:44pm

		PizzaFactory.getPizza(meatLoversCode, quantity, orderTime, deliveryTime);
	}

	/**
	 * Test getPizza function where order time is before 7pm
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void beforeOpenOrderGetPizzaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(18, 59); // 6:59pm
		deliveryTime = LocalTime.of(19, 30); // 7:30pm

		PizzaFactory.getPizza(meatLoversCode, quantity, orderTime, deliveryTime);
	}

	/**
	 * Test getPizza function where order time is after 10:59pm
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void afterCloseOrderGetPizzaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(23, 0); // 11:00pm
		deliveryTime = LocalTime.of(23, 30); // 11:30pm

		PizzaFactory.getPizza(meatLoversCode, quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor does not accept am values
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void checkPMGetPizzaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(7, 0); // 7:00am
		deliveryTime = LocalTime.of(7, 30); // 7:30am

		PizzaFactory.getPizza(meatLoversCode, quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor where delivery time is before order time
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void beforeOrderDeliverGetPizzaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(20, 21); // 8:21pm
		deliveryTime = LocalTime.of(20, 0); // 8:00pm

		PizzaFactory.getPizza(meatLoversCode, quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor where delivery time faster than place can cook food
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void beforeMinTimeDeliverGetPizzaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(20, 21); // 8:21pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		PizzaFactory.getPizza(meatLoversCode, quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor where delivery time is the same as the order time
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void orderSameAsDeliverGetPizzaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(20, 21); // 8:21pm
		deliveryTime = LocalTime.of(20, 21); // 8:21pm

		PizzaFactory.getPizza(meatLoversCode, quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor where deliver time is before 7pm
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void beforeOpenDeliverGetPizzaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(18, 59); // 6:59pm

		PizzaFactory.getPizza(meatLoversCode, quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor where deliver time is after 12:08pm
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void afterCloseDeliverGetPizzaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(22, 59); // 10:59pm
		deliveryTime = LocalTime.of(0, 9); // 12:09pm

		PizzaFactory.getPizza(meatLoversCode, quantity, orderTime, deliveryTime);
	}
}
