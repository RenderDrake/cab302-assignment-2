package asgn2Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalTime;

import org.junit.Test;

import asgn2Exceptions.PizzaException;
import asgn2Pizzas.MargheritaPizza;
import asgn2Pizzas.MeatLoversPizza;
import asgn2Pizzas.PizzaTopping;
import asgn2Pizzas.VegetarianPizza;

//TODO Maybe impliment a test to check the order that calculateCostPerPizza is used
//TODO check that type is one of the three codes

/**
 * A class that that tests the asgn2Pizzas.MargheritaPizza,
 * asgn2Pizzas.VegetarianPizza, asgn2Pizzas.MeatLoversPizza classes. Note that
 * an instance of asgn2Pizzas.MeatLoversPizza should be used to test the
 * functionality of the asgn2Pizzas.Pizza abstract class.
 * 
 * @author Charles Alleman
 *
 */
public class PizzaTests {
	private int quantity;
	private final double acceptableDifferenceBetweenPrices = 0.001;
	private final double costOfMeatLovers = 5.00;
	private final double costOfMargherita = 1.50;
	private final double costOfVegetarian = 5.50;
	private final double priceOfMeatLovers = 12.00;
	private final double priceOfMargherita = 8.00;
	private final double priceOfVegetarian = 10.00;
	private LocalTime orderTime;
	private LocalTime deliveryTime;
	private MargheritaPizza maPizza;
	private MeatLoversPizza mlPizza;
	private VegetarianPizza vPizza;

	/*
	 * Exception Testing
	 */

	/**
	 * Test constructor crashes with 0 pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void zeroMeatLoversConstructorTest() throws PizzaException {
		quantity = 0;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes with negative pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void negativeMeatLoversConstructorTest() throws PizzaException {
		quantity = -1;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes with over 10 pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void excessMeatLoversConstructorTest() throws PizzaException {
		quantity = 11;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes if order time is before 7pm
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void beforeOpenOrderMeatLoversConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(18, 59); // 6:59pm
		deliveryTime = LocalTime.of(19, 30); // 7:30pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes if order time is after 11:00pm
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void afterCloseOrderMeatLoversConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(23, 1); // 11:01pm
		deliveryTime = LocalTime.of(23, 30); // 11:30pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor does not accept am values
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void checkPMMeatLoversConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(8, 0); // 7:00am
		deliveryTime = LocalTime.of(8, 30); // 7:30am

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes if delivery time is before order time
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void beforeOrderDeliverMeatLoversConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(22, 30); // 10:30pm
		deliveryTime = LocalTime.of(22, 29); // 10:29pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes if delivery time faster than place can cook food
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void beforeMinTimeDeliverMeatLoversConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 1); // 8:09pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes if delivery time is the same as the order time
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void orderSameAsDeliverMeatLoversConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 0); // 8pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes if deliver time is before 7pm
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void beforeOpenDeliverMeatLoversConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(18, 59); // 6:59pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes if deliver time is after 12:00pm
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void afterCloseDeliverMeatLoversConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(23, 0); // 11:00pm
		deliveryTime = LocalTime.of(0, 11); // 12:01pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes with 0 pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void zeroMargheritaConstructorTest() throws PizzaException {
		quantity = 0;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes with negative pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void negativeMargheritaConstructorTest() throws PizzaException {
		quantity = -1;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes with over 10 pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void excessMargheritaConstructorTest() throws PizzaException {
		quantity = 11;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes if order time is before 7pm
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void beforeOpenOrderMargheritaConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(18, 59); // 6:59pm
		deliveryTime = LocalTime.of(19, 30); // 7:30pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes if order time is after 11:00pm
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void afterCloseOrderMargheritaConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(23, 1); // 11:01pm
		deliveryTime = LocalTime.of(23, 30); // 11:30pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor does not accept am values
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void checkPMMargheritaConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(8, 0); // 7:00am
		deliveryTime = LocalTime.of(8, 30); // 7:30am

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes if delivery time is before order time
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void beforeOrderDeliverMargheritaConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(22, 30); // 10:30pm
		deliveryTime = LocalTime.of(22, 29); // 10:29pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes if delivery time faster than place can cook food
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void beforeMinTimeDeliverMargheritaConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 1); // 8:09pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes if delivery time is the same as the order time
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void orderSameAsDeliverMargheritaConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 0); // 8pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes if deliver time is before 7pm
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void beforeOpenDeliverMargheritaConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(18, 59); // 6:59pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes if deliver time is after 12:01pm
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void afterCloseDeliverMargheritaConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(23, 0); // 11:00pm
		deliveryTime = LocalTime.of(0, 11); // 12:01pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes with 0 pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void zeroVegetarianConstructorTest() throws PizzaException {
		quantity = 0;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes with negative pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void negativeVegetarianConstructorTest() throws PizzaException {
		quantity = -1;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes with over 10 pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void excessVegetarianConstructorTest() throws PizzaException {
		quantity = 11;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes if order time is before 7pm
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void beforeOpenOrderVegetarianConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(18, 59); // 6:59pm
		deliveryTime = LocalTime.of(19, 30); // 7:30pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes if order time is after 11:00pm
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void afterCloseOrderVegetarianConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(23, 1); // 11:01pm
		deliveryTime = LocalTime.of(23, 30); // 11:30pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor does not accept am values
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void checkPMVegetarianConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(8, 0); // 7:00am
		deliveryTime = LocalTime.of(8, 30); // 7:30am

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes if delivery time is before order time
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void beforeOrderDeliverVegetarianConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(22, 30); // 10:30pm
		deliveryTime = LocalTime.of(22, 29); // 10:29pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes if delivery time faster than place can cook food
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void beforeMinTimeDeliverVegetarianConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 1); // 8:09pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes if delivery time is the same as the order time
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void orderSameAsDeliverVegetarianConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(20, 0); // 8pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes if deliver time is before 7pm
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void beforeOpenDeliverVegetarianConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(20, 0); // 8pm
		deliveryTime = LocalTime.of(18, 59); // 6:59pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor crashes if deliver time is after 12:11pm
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = PizzaException.class)
	public void afterCloseDeliverVegetarianConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(23, 0); // 11:00pm
		deliveryTime = LocalTime.of(0, 11); // 12:01pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
	}

	/*
	 * Non-Exception Testing
	 */

	/**
	 * Test constructor does not crash
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void MeatLoversConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(20, 0); // 8:00pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works with one pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneMeatLoversConstructorTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(20, 0); // 8:00pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works with multiple pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleMeatLoversConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(20, 0); // 8:00pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works with maximum number of pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void maxMeatLoversConstructorTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(20, 0); // 8:00pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works at opening time
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void openOrderMeatLoversConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(19, 0); // 7:00pm
		deliveryTime = LocalTime.of(19, 30); // 7:30pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works with a random time in the working hours
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void workingHoursOrderMeatLoversConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works at the closing time
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void closingOrderMeatLoversConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(23, 0); // 11:00pm
		deliveryTime = LocalTime.of(23, 30); // 11:30pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works where the delivery time is 10 minutes after order
	 * time
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void fastestOrderMeatLoversConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 31); // 9:31pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works where the delivery time is 1 hour after order time
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void longestOrderMeatLoversConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(22, 21); // 10:21pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works for the latest possible order time possible
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void latestOrderMeatLoversConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(23, 0); // 11:00pm
		deliveryTime = LocalTime.of(0, 0); // 12:00pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Determine if function runs without crashing for one pizza and updates
	 * cost to correct value
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneCalculateCostPerPizzaMeatLoversTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
		mlPizza.calculateCostPerPizza();
		assertEquals(costOfMeatLovers, mlPizza.getCostPerPizza(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Determine if function runs without crashing for multiple pizzas and
	 * updates cost to correct value
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleCalculateCostPerPizzaMeatLoversTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
		mlPizza.calculateCostPerPizza();
		assertEquals(costOfMeatLovers, mlPizza.getCostPerPizza(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Determine if function runs without crashing for max number of pizzas and
	 * updates cost to correct value
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void maxCalculateCostPerPizzaMeatLoversTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
		mlPizza.calculateCostPerPizza();
		assertEquals(costOfMeatLovers, mlPizza.getCostPerPizza(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check price per pizza when one pizza is entered
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneGetPricePerPizzaMeatLoversTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
		assertEquals(priceOfMeatLovers, mlPizza.getPricePerPizza(), acceptableDifferenceBetweenPrices);

	}

	/**
	 * Check price per pizza when multiple pizzas are entered
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleGetPricePerPizzaMeatLoversTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
		assertEquals(priceOfMeatLovers, mlPizza.getPricePerPizza(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check price per pizza when the maximum number of pizzas are entered
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void maxGetPricePerPizzaMeatLoversTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
		assertEquals(priceOfMeatLovers, mlPizza.getPricePerPizza(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check cost to create pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneGetCostPerPizzaMeatLoversTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
		mlPizza.calculateCostPerPizza();
		assertEquals(costOfMeatLovers, mlPizza.getCostPerPizza(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check cost to create pizza when multiple pizzas are entered
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleGetCostPerPizzaMeatLoversTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
		mlPizza.calculateCostPerPizza();
		assertEquals(costOfMeatLovers, mlPizza.getCostPerPizza(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check cost to create pizza when the max number of pizzas are entered
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void maxGetCostPerPizzaMeatLoversTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
		mlPizza.calculateCostPerPizza();
		assertEquals(costOfMeatLovers, mlPizza.getCostPerPizza(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check cost to create one pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneGetOrderCostMeatLoversTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderCost = costOfMeatLovers * quantity;

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
		mlPizza.calculateCostPerPizza();
		assertEquals(orderCost, mlPizza.getOrderCost(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check cost to create multiple pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleGetOrderCostMeatLoversTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderCost = costOfMeatLovers * quantity;

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
		mlPizza.calculateCostPerPizza();
		assertEquals(orderCost, mlPizza.getOrderCost(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check cost to create maximum number of pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void maxGetOrderCostMeatLoversTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderCost = costOfMeatLovers * quantity;

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
		mlPizza.calculateCostPerPizza();
		assertEquals(orderCost, mlPizza.getOrderCost(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check price of one pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneGetOrderPriceMeatLoversTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderPrice = priceOfMeatLovers * quantity;

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
		mlPizza.calculateCostPerPizza();
		assertEquals(orderPrice, mlPizza.getOrderPrice(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check price of multiple pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleGetOrderPriceMeatLoversTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderPrice = priceOfMeatLovers * quantity;

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
		mlPizza.calculateCostPerPizza();
		assertEquals(orderPrice, mlPizza.getOrderPrice(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check price of the maximum number of pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void maxGetOrderPriceMeatLoversTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderPrice = priceOfMeatLovers * quantity;

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
		mlPizza.calculateCostPerPizza();
		assertEquals(orderPrice, mlPizza.getOrderPrice(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check the profit returned by one pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneGetOrderProfitPizzaMeatLoversTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderProfit = (priceOfMeatLovers - costOfMeatLovers) * quantity;

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
		mlPizza.calculateCostPerPizza();
		assertEquals(orderProfit, mlPizza.getOrderProfit(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check the profit returned by multiple pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleGetOrderProfitPizzaMeatLoversTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderProfit = (priceOfMeatLovers - costOfMeatLovers) * quantity;

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
		mlPizza.calculateCostPerPizza();
		assertEquals(orderProfit, mlPizza.getOrderProfit(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check the profit returned by the max amount of pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void maxGetOrderProfitPizzaMeatLoversTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderProfit = (priceOfMeatLovers - costOfMeatLovers) * quantity;

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
		mlPizza.calculateCostPerPizza();
		assertEquals(orderProfit, mlPizza.getOrderProfit(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check the type of a pizza is not whitespace or empty
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void getTypeMeatLoversTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
		assertTrue(mlPizza.getPizzaType().trim().length() > 0);
	}

	/**
	 * Checks that pizza contains correct toppings
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void containsToppingMeatLoversTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);

		// Check list of ingredients
		assertTrue(mlPizza.containsTopping(PizzaTopping.CHEESE));
		assertTrue(mlPizza.containsTopping(PizzaTopping.TOMATO));
		assertTrue(mlPizza.containsTopping(PizzaTopping.BACON));
		assertTrue(mlPizza.containsTopping(PizzaTopping.SALAMI));
		assertTrue(mlPizza.containsTopping(PizzaTopping.PEPPERONI));
		assertFalse(mlPizza.containsTopping(PizzaTopping.CAPSICUM));
		assertFalse(mlPizza.containsTopping(PizzaTopping.MUSHROOM));
		assertFalse(mlPizza.containsTopping(PizzaTopping.EGGPLANT));
	}

	/**
	 * Get quantity of pizzas for one pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneGetQuantityMeatLoversTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
		assertEquals(quantity, mlPizza.getQuantity());
	}

	/**
	 * Get quantity of pizzas for multiple pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleGetQuantityMeatLoversTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
		assertEquals(quantity, mlPizza.getQuantity());
	}

	/**
	 * Get quantity of pizzas for maximum number of pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void maxGetQuantityMeatLoversTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		mlPizza = new MeatLoversPizza(quantity, orderTime, deliveryTime);
		assertEquals(quantity, mlPizza.getQuantity());
	}

	/**
	 * Test constructor does not crash
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void MargheritaConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(20, 0); // 8:00pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works with one pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneMargheritaConstructorTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(20, 0); // 8:00pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works with multiple pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleMargheritaConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(20, 0); // 8:00pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works with maximum number of pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void maxMargheritaConstructorTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(20, 0); // 8:00pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works at opening time
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void openOrderMargheritaConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(19, 0); // 7:00pm
		deliveryTime = LocalTime.of(19, 30); // 7:30pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works with a random time in the working hours
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void workingHoursOrderMargheritaConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works at the closing time
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void closingOrderMargheritaConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(23, 0); // 11:00pm
		deliveryTime = LocalTime.of(23, 30); // 11:30pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works where the delivery time is 10 minutes after order
	 * time
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void fastestOrderMargheritaConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 31); // 9:31pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works where the delivery time is 1 hour after order time
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void longestOrderMargheritaConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(22, 21); // 10:21pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works for the latest possible order time possible
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void latestOrderMargheritaConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(23, 0); // 11:00pm
		deliveryTime = LocalTime.of(0, 0); // 12:00pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Determine if function runs without crashing for one pizza and updates
	 * cost to correct value
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneCalculateCostPerPizzaMargheritaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
		maPizza.calculateCostPerPizza();
		assertEquals(costOfMargherita, maPizza.getCostPerPizza(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Determine if function runs without crashing for multiple pizzas and
	 * updates cost to correct value
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleCalculateCostPerPizzaMargheritaTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
		maPizza.calculateCostPerPizza();
		assertEquals(costOfMargherita, maPizza.getCostPerPizza(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Determine if function runs without crashing for max number of pizzas and
	 * updates cost to correct value
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void maxCalculateCostPerPizzaMargheritaTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
		maPizza.calculateCostPerPizza();
		assertEquals(costOfMargherita, maPizza.getCostPerPizza(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check price per pizza when one pizza is entered
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneGetPricePerPizzaMargheritaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
		assertEquals(priceOfMargherita, maPizza.getPricePerPizza(), acceptableDifferenceBetweenPrices);

	}

	/**
	 * Check price per pizza when multiple pizzas are entered
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleGetPricePerPizzaMargheritaTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
		assertEquals(priceOfMargherita, maPizza.getPricePerPizza(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check price per pizza when the maximum number of pizzas are entered
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void maxGetPricePerPizzaMargheritaTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
		assertEquals(priceOfMargherita, maPizza.getPricePerPizza(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check cost to create pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneGetCostPerPizzaMargheritaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
		maPizza.calculateCostPerPizza();
		assertEquals(costOfMargherita, maPizza.getCostPerPizza(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check cost to create pizza when multiple pizzas are entered
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleGetCostPerPizzaMargheritaTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
		maPizza.calculateCostPerPizza();
		assertEquals(costOfMargherita, maPizza.getCostPerPizza(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check cost to create pizza when the max number of pizzas are entered
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void maxGetCostPerPizzaMargheritaTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
		maPizza.calculateCostPerPizza();
		assertEquals(costOfMargherita, maPizza.getCostPerPizza(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check cost to create one pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneGetOrderCostMargheritaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderCost = costOfMargherita * quantity;

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
		maPizza.calculateCostPerPizza();
		assertEquals(orderCost, maPizza.getOrderCost(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check cost to create multiple pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleGetOrderCostMargheritaTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderCost = costOfMargherita * quantity;

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
		maPizza.calculateCostPerPizza();
		assertEquals(orderCost, maPizza.getOrderCost(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check cost to create maximum number of pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void maxGetOrderCostMargheritaTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderCost = costOfMargherita * quantity;

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
		maPizza.calculateCostPerPizza();
		assertEquals(orderCost, maPizza.getOrderCost(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check price of one pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneGetOrderPriceMargheritaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderPrice = priceOfMargherita * quantity;

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
		maPizza.calculateCostPerPizza();
		assertEquals(orderPrice, maPizza.getOrderPrice(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check price of multiple pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleGetOrderPriceMargheritaTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderPrice = priceOfMargherita * quantity;

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
		maPizza.calculateCostPerPizza();
		assertEquals(orderPrice, maPizza.getOrderPrice(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check price of the maximum number of pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void maxGetOrderPriceMargheritaTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderPrice = priceOfMargherita * quantity;

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
		maPizza.calculateCostPerPizza();
		assertEquals(orderPrice, maPizza.getOrderPrice(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check the profit returned by one pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneGetOrderProfitPizzaMargheritaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderProfit = (priceOfMargherita - costOfMargherita) * quantity;

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
		maPizza.calculateCostPerPizza();
		assertEquals(orderProfit, maPizza.getOrderProfit(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check the profit returned by multiple pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleGetOrderProfitPizzaMargheritaTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderProfit = (priceOfMargherita - costOfMargherita) * quantity;

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
		maPizza.calculateCostPerPizza();
		assertEquals(orderProfit, maPizza.getOrderProfit(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check the profit returned by the max amount of pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void maxGetOrderProfitPizzaMargheritaTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderProfit = (priceOfMargherita - costOfMargherita) * quantity;

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
		maPizza.calculateCostPerPizza();
		assertEquals(orderProfit, maPizza.getOrderProfit(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check the type of a pizza is not whitespace or empty
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void getTypeMargheritaTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
		assertTrue(maPizza.getPizzaType().trim().length() > 0);
	}

	/**
	 * Checks that pizza contains correct toppings
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void containsToppingMargheritaTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);

		// Check list of ingredients
		assertTrue(maPizza.containsTopping(PizzaTopping.CHEESE));
		assertTrue(maPizza.containsTopping(PizzaTopping.TOMATO));
		assertFalse(maPizza.containsTopping(PizzaTopping.BACON));
		assertFalse(maPizza.containsTopping(PizzaTopping.SALAMI));
		assertFalse(maPizza.containsTopping(PizzaTopping.PEPPERONI));
		assertFalse(maPizza.containsTopping(PizzaTopping.CAPSICUM));
		assertFalse(maPizza.containsTopping(PizzaTopping.MUSHROOM));
		assertFalse(maPizza.containsTopping(PizzaTopping.EGGPLANT));
	}

	/**
	 * Get quantity of pizzas for one pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneGetQuantityMargheritaTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
		assertEquals(quantity, maPizza.getQuantity());
	}

	/**
	 * Get quantity of pizzas for multiple pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleGetQuantityMargheritaTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
		assertEquals(quantity, maPizza.getQuantity());
	}

	/**
	 * Get quantity of pizzas for maximum number of pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void maxGetQuantityMargheritaTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		maPizza = new MargheritaPizza(quantity, orderTime, deliveryTime);
		assertEquals(quantity, maPizza.getQuantity());
	}

	/**
	 * Test constructor does not crash
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void VegetarianConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(20, 0); // 8:00pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works with one pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneVegetarianConstructorTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(20, 0); // 8:00pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works with multiple pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleVegetarianConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(20, 0); // 8:00pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works with maximum number of pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void maxVegetarianConstructorTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(20, 0); // 8:00pm
		deliveryTime = LocalTime.of(20, 30); // 8:30pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works at opening time
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void openOrderVegetarianConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(19, 0); // 7:00pm
		deliveryTime = LocalTime.of(19, 30); // 7:30pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works with a random time in the working hours
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void workingHoursOrderVegetarianConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works at the closing time
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void closingOrderVegetarianConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(23, 0); // 11:00pm
		deliveryTime = LocalTime.of(23, 30); // 11:30pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works where the delivery time is 10 minutes after order
	 * time
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void fastestOrderVegetarianConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 31); // 9:31pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works where the delivery time is 1 hour after order time
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void longestOrderVegetarianConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(22, 21); // 10:21pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Test constructor works for the latest possible order time possible
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void latestOrderVegetarianConstructorTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(23, 0); // 11:00pm
		deliveryTime = LocalTime.of(0, 0); // 12:00pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
	}

	/**
	 * Determine if function runs without crashing for one pizza and updates
	 * cost to correct value
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneCalculateCostPerPizzaVegetarianTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
		vPizza.calculateCostPerPizza();
		assertEquals(costOfVegetarian, vPizza.getCostPerPizza(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Determine if function runs without crashing for multiple pizzas and
	 * updates cost to correct value
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleCalculateCostPerPizzaVegetarianTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
		vPizza.calculateCostPerPizza();
		assertEquals(costOfVegetarian, vPizza.getCostPerPizza(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Determine if function runs without crashing for max number of pizzas and
	 * updates cost to correct value
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void maxCalculateCostPerPizzaVegetarianTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
		vPizza.calculateCostPerPizza();
		assertEquals(costOfVegetarian, vPizza.getCostPerPizza(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check price per pizza when one pizza is entered
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneGetPricePerPizzaVegetarianTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
		assertEquals(priceOfVegetarian, vPizza.getPricePerPizza(), acceptableDifferenceBetweenPrices);

	}

	/**
	 * Check price per pizza when multiple pizzas are entered
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleGetPricePerPizzaVegetarianTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
		assertEquals(priceOfVegetarian, vPizza.getPricePerPizza(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check price per pizza when the maximum number of pizzas are entered
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void maxGetPricePerPizzaVegetarianTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
		assertEquals(priceOfVegetarian, vPizza.getPricePerPizza(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check cost to create pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneGetCostPerPizzaVegetarianTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
		vPizza.calculateCostPerPizza();
		assertEquals(costOfVegetarian, vPizza.getCostPerPizza(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check cost to create pizza when multiple pizzas are entered
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleGetCostPerPizzaVegetarianTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
		vPizza.calculateCostPerPizza();
		assertEquals(costOfVegetarian, vPizza.getCostPerPizza(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check cost to create pizza when the max number of pizzas are entered
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void maxGetCostPerPizzaVegetarianTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
		vPizza.calculateCostPerPizza();
		assertEquals(costOfVegetarian, vPizza.getCostPerPizza(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check cost to create one pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneGetOrderCostVegetarianTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderCost = costOfVegetarian * quantity;

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
		vPizza.calculateCostPerPizza();
		assertEquals(orderCost, vPizza.getOrderCost(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check cost to create multiple pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleGetOrderCostVegetarianTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderCost = costOfVegetarian * quantity;

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
		vPizza.calculateCostPerPizza();
		assertEquals(orderCost, vPizza.getOrderCost(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check cost to create maximum number of pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void maxGetOrderCostVegetarianTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderCost = costOfVegetarian * quantity;

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
		vPizza.calculateCostPerPizza();
		assertEquals(orderCost, vPizza.getOrderCost(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check price of one pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneGetOrderPriceVegetarianTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderPrice = priceOfVegetarian * quantity;

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
		vPizza.calculateCostPerPizza();
		assertEquals(orderPrice, vPizza.getOrderPrice(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check price of multiple pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleGetOrderPriceVegetarianTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderPrice = priceOfVegetarian * quantity;

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
		vPizza.calculateCostPerPizza();
		assertEquals(orderPrice, vPizza.getOrderPrice(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check price of the maximum number of pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void maxGetOrderPriceVegetarianTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderPrice = priceOfVegetarian * quantity;

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
		vPizza.calculateCostPerPizza();
		assertEquals(orderPrice, vPizza.getOrderPrice(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check the profit returned by one pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneGetOrderProfitPizzaVegetarianTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderProfit = (priceOfVegetarian - costOfVegetarian) * quantity;

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
		vPizza.calculateCostPerPizza();
		assertEquals(orderProfit, vPizza.getOrderProfit(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check the profit returned by multiple pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleGetOrderProfitPizzaVegetarianTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderProfit = (priceOfVegetarian - costOfVegetarian) * quantity;

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
		vPizza.calculateCostPerPizza();
		assertEquals(orderProfit, vPizza.getOrderProfit(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check the profit returned by the max amount of pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void maxGetOrderProfitPizzaVegetarianTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm
		double orderProfit = (priceOfVegetarian - costOfVegetarian) * quantity;

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
		vPizza.calculateCostPerPizza();
		assertEquals(orderProfit, vPizza.getOrderProfit(), acceptableDifferenceBetweenPrices);
	}

	/**
	 * Check the type of a pizza is not whitespace or empty
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void getTypeVegetarianTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
		assertTrue(vPizza.getPizzaType().trim().length() > 0);
	}

	/**
	 * Checks that pizza contains correct toppings
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void containsToppingVegetarianTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);

		// Check list of ingredients
		assertTrue(vPizza.containsTopping(PizzaTopping.CHEESE));
		assertTrue(vPizza.containsTopping(PizzaTopping.TOMATO));
		assertFalse(vPizza.containsTopping(PizzaTopping.BACON));
		assertFalse(vPizza.containsTopping(PizzaTopping.SALAMI));
		assertFalse(vPizza.containsTopping(PizzaTopping.PEPPERONI));
		assertTrue(vPizza.containsTopping(PizzaTopping.CAPSICUM));
		assertTrue(vPizza.containsTopping(PizzaTopping.MUSHROOM));
		assertTrue(vPizza.containsTopping(PizzaTopping.EGGPLANT));
	}

	/**
	 * Get quantity of pizzas for one pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void oneGetQuantityVegetarianTest() throws PizzaException {
		quantity = 1;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
		assertEquals(quantity, vPizza.getQuantity());
	}

	/**
	 * Get quantity of pizzas for multiple pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void multipleGetQuantityVegetarianTest() throws PizzaException {
		quantity = 5;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
		assertEquals(quantity, vPizza.getQuantity());
	}

	/**
	 * Get quantity of pizzas for maximum number of pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test
	public void maxGetQuantityVegetarianTest() throws PizzaException {
		quantity = 10;
		orderTime = LocalTime.of(21, 21); // 9:21pm
		deliveryTime = LocalTime.of(21, 44); // 9:44pm

		vPizza = new VegetarianPizza(quantity, orderTime, deliveryTime);
		assertEquals(quantity, vPizza.getQuantity());
	}
}
