package asgn2Tests;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Test;

import asgn2Customers.DriverDeliveryCustomer;
import asgn2Customers.DroneDeliveryCustomer;
import asgn2Customers.PickUpCustomer;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Restaurant.LogHandler;

/**
 * A class that tests the methods relating to the creation of Customer objects
 * in the asgn2Restaurant.LogHander class.
 *
 * @author Sylvester Soetianto
 */
public class LogHandlerCustomerTests {
	private File file;
	private final String p_code = "PZL";
	private final String quantity = "1";
	private final String driverCode = "DVC", driverSimilar = "dvc", driverExcess = "DVCC", droneCode = "DNC",
			pickCode = "PUC", whiteCode = "   ", customerName = "Dummy", nothing = "",
			whiteSpaces = "                    ", excessName = "StuffStuffStuffStuffA", customerMobile = "0123456789",
			wrongMobile = "1123456789", alphaMobile = "abcdefghjk", whiteMobile = "          ",
			excessMobile = "01234567890", shortMobile = "012345678";
	private final Integer locationX = 5, locationY = 5;
	private String orderTime = "19:00:00";
	private final String deliverTime = "19:20:00";
	private String name = customerName;
	private String mobile = customerMobile;
	private String c_code = driverCode;
	private String x_location = locationX.toString();
	private String y_location = locationY.toString();
	private String sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + ","
			+ x_location + "," + y_location + "," + p_code + "," + quantity;
	private DriverDeliveryCustomer driverA;
	private DroneDeliveryCustomer droneB;
	private PickUpCustomer pickC;

	/**
	 * Test creation of a valid DriverDeliveryCustomer.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test
	public void testCreateDriverDeliveryCustomer() throws CustomerException, LogHandlerException {
		driverA = new DriverDeliveryCustomer(name, mobile, locationX, locationY);
		assertTrue(driverA.equals(LogHandler.createCustomer(sentence)));
	}

	/**
	 * Test creation of a valid DroneDeliveryCustomer.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test
	public void testCreateDroneDeliveryCustomer() throws CustomerException, LogHandlerException {
		c_code = droneCode;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		droneB = new DroneDeliveryCustomer(name, mobile, locationX, locationY);
		assertTrue(droneB.equals(LogHandler.createCustomer(sentence)));
	}

	/**
	 * Test creation of a valid PickUpCustomer.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test
	public void testCreatePickUpCustomer() throws CustomerException, LogHandlerException {
		c_code = pickCode;
		x_location = "0";
		y_location = "0";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		pickC = new PickUpCustomer(name, mobile, 0, 0);
		assertTrue(pickC.equals(LogHandler.createCustomer(sentence)));
	}

	/**
	 * Test creation of a DriverDeliveryCustomer with no order time.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreateDriverDeliveryCustomerNoOrderTime() throws CustomerException, LogHandlerException {
		orderTime = nothing;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DriverDeliveryCustomer with no name.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreateDriverDeliveryCustomerNoName() throws CustomerException, LogHandlerException {
		name = nothing;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DriverDeliveryCustomer with an excess name.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreateDriverDeliveryCustomerExcessName() throws CustomerException, LogHandlerException {
		name = excessName;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DriverDeliveryCustomer with a whitespace name.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreateDriverDeliveryCustomerWhiteSpaceName() throws CustomerException, LogHandlerException {
		name = whiteSpaces;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DriverDeliveryCustomer with no mobile.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreateDriverDeliveryCustomerNoMobile() throws CustomerException, LogHandlerException {
		mobile = nothing;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DriverDeliveryCustomer with an invalid mobile.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreateDriverDeliveryCustomerInvalidMobile() throws CustomerException, LogHandlerException {
		mobile = wrongMobile;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DriverDeliveryCustomer with an excess mobile.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreateDriverDeliveryCustomerExcessMobile() throws CustomerException, LogHandlerException {
		mobile = excessMobile;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DriverDeliveryCustomer with a whitespace mobile.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreateDriverDeliveryCustomerWhiteSpaceMobile() throws CustomerException, LogHandlerException {
		mobile = whiteMobile;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DriverDeliveryCustomer with an excess mobile.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreateDriverDeliveryCustomerInsufficientMobile() throws CustomerException, LogHandlerException {
		mobile = shortMobile;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DriverDeliveryCustomer with a short mobile.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreateDriverDeliveryCustomerNoDigitsMobile() throws CustomerException, LogHandlerException {
		mobile = alphaMobile;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a customer with no customer code.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreateNoCustomerCode() throws CustomerException, LogHandlerException {
		c_code = nothing;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a customer with an excess customer code.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreateExcessCustomerCode() throws CustomerException, LogHandlerException {
		c_code = driverExcess;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a customer with a whitespace customer code.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreateDriverDeliveryCustomerWhiteSpaceCustomerCode() throws CustomerException, LogHandlerException {
		c_code = whiteCode;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a customer with a similar customer code.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreateDriverDeliveryCustomerSimilarCustomerCode() throws CustomerException, LogHandlerException {
		c_code = driverSimilar;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DriverDeliveryCustomer without a location x.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreateDriverDeliveryCustomerNoLocationX() throws CustomerException, LogHandlerException {
		x_location = nothing;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DriverDeliveryCustomer with a negative excess location
	 * x.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreateDriverDeliveryCustomerNegExcessLocationX() throws CustomerException, LogHandlerException {
		x_location = "-11";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DriverDeliveryCustomer with a zero location x.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test
	public void testCreateDriverDeliveryCustomerZeroLocationX() throws CustomerException, LogHandlerException {
		x_location = "0";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DriverDeliveryCustomer with an excess location x.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreateDriverDeliveryCustomerExcessLocationX() throws CustomerException, LogHandlerException {
		x_location = "11";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DriverDeliveryCustomer without a location y.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreateDriverDeliveryCustomerNoLocationY() throws CustomerException, LogHandlerException {
		y_location = nothing;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DriverDeliveryCustomer with a negative excess location
	 * y.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreateDriverDeliveryCustomerNegExcessLocationY() throws CustomerException, LogHandlerException {
		y_location = "-11";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DriverDeliveryCustomer with a zero location y.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test
	public void testCreateDriverDeliveryCustomerZeroLocationY() throws CustomerException, LogHandlerException {
		y_location = "0";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DriverDeliveryCustomer with an excess location y.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreateDriverDeliveryCustomerExcessLocationY() throws CustomerException, LogHandlerException {
		y_location = "11";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DriverDeliveryCustomer with a negative excess distance
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreateDriverDeliveryCustomerNegExcess() throws CustomerException, LogHandlerException {
		x_location = "-11";
		y_location = "-11";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DriverDeliveryCustomer with a no distance
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreateDriverDeliveryCustomerZeroDistance() throws CustomerException, LogHandlerException {
		x_location = "0";
		y_location = "0";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DriverDeliveryCustomer with an excess distance
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreateDriverDeliveryCustomerExcessDistance() throws CustomerException, LogHandlerException {
		x_location = "11";
		y_location = "11";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DroneDeliveryCustomer with no name.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreateDroneDeliveryCustomerNoName() throws CustomerException, LogHandlerException {
		c_code = droneCode;
		name = nothing;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DroneDeliveryCustomer with an excess name.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreateDroneDeliveryCustomerExcessName() throws CustomerException, LogHandlerException {
		c_code = droneCode;
		name = excessName;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DroneDeliveryCustomer with a whitespace name.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreateDroneDeliveryCustomerWhiteSpaceName() throws CustomerException, LogHandlerException {
		c_code = droneCode;
		name = whiteSpaces;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DroneDeliveryCustomer without a mobile.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreateDroneDeliveryCustomerNoMobile() throws CustomerException, LogHandlerException {
		c_code = droneCode;
		mobile = nothing;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DroneDeliveryCustomer with an invalid mobile.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreateDroneDeliveryCustomerInvalidMobile() throws CustomerException, LogHandlerException {
		c_code = droneCode;
		mobile = wrongMobile;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DroneDeliveryCustomer with an excess mobile.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreateDroneDeliveryCustomerExcessMobile() throws CustomerException, LogHandlerException {
		c_code = droneCode;
		mobile = wrongMobile;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DroneDeliveryCustomer with a whitespace mobile.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreateDroneDeliveryCustomerWhiteSpaceMobile() throws CustomerException, LogHandlerException {
		c_code = droneCode;
		mobile = whiteMobile;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DroneDeliveryCustomer with a mobile of incorrect digit
	 * length.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreateDroneDeliveryCustomerInsufficientMobile() throws CustomerException, LogHandlerException {
		c_code = droneCode;
		mobile = shortMobile;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DroneDeliveryCustomer with a mobile of no digits.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreateDroneDeliveryCustomerNoDigitsMobile() throws CustomerException, LogHandlerException {
		c_code = droneCode;
		mobile = alphaMobile;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DroneDeliveryCustomer with no location x.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreateDroneDeliveryCustomerNoLocationX() throws CustomerException, LogHandlerException {
		c_code = droneCode;
		x_location = nothing;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DroneDeliveryCustomer with a negative excess location
	 * x.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreateDroneDeliveryCustomerNegExcessLocationX() throws CustomerException, LogHandlerException {
		c_code = droneCode;
		x_location = "-11";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DroneDeliveryCustomer with a zero location x.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test
	public void testCreateDroneDeliveryCustomerZeroLocationX() throws CustomerException, LogHandlerException {
		c_code = droneCode;
		x_location = "0";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DroneDeliveryCustomer with an excess location x.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreateDroneDeliveryCustomerExcessLocationX() throws CustomerException, LogHandlerException {
		c_code = droneCode;
		x_location = "11";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DroneDeliveryCustomer with no location y.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreateDroneDeliveryCustomerNoLocationY() throws CustomerException, LogHandlerException {
		c_code = droneCode;
		y_location = nothing;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DroneDeliveryCustomer with a negative excess location
	 * y.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreateDroneDeliveryCustomerNegExcessLocationY() throws CustomerException, LogHandlerException {
		c_code = droneCode;
		y_location = "-11";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DroneDeliveryCustomer with a zero location y.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test
	public void testCreateDroneDeliveryCustomerZeroLocationY() throws CustomerException, LogHandlerException {
		c_code = droneCode;
		y_location = "0";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DroneDeliveryCustomer with an excess location y.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreateDroneDeliveryCustomerExcessLocationY() throws CustomerException, LogHandlerException {
		c_code = droneCode;
		y_location = "11";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DroneDeliveryCustomer with a negative excess distance.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreateDroneDeliveryCustomerNegExcess() throws CustomerException, LogHandlerException {
		c_code = droneCode;
		x_location = "-11";
		y_location = "-11";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DroneDeliveryCustomer with no distance.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreateDroneDeliveryCustomerZeroDistance() throws CustomerException, LogHandlerException {
		c_code = droneCode;
		x_location = "0";
		y_location = "0";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a DroneDeliveryCustomer with an excess distance.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreateDroneDeliveryCustomerExcessDistance() throws CustomerException, LogHandlerException {
		c_code = droneCode;
		x_location = "11";
		y_location = "11";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a PickUpCustomer without a name.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreatePickUpCustomerNoName() throws CustomerException, LogHandlerException {
		c_code = pickCode;
		name = nothing;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a PickUpCustomer with an excess name.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreatePickUpCustomerExcessName() throws CustomerException, LogHandlerException {
		name = "DummyDummyDummyDummyC";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a PickUpCustomer with a whitespace name.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreatePickUpCustomerWhiteSpaceName() throws CustomerException, LogHandlerException {
		c_code = pickCode;
		name = whiteSpaces;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a PickUpCustomer with no mobile.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreatePickUpCustomerNoMobile() throws CustomerException, LogHandlerException {
		c_code = pickCode;
		mobile = nothing;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a PickUpCustomer with an invalid mobile.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreatePickUpCustomerInvalidMobile() throws CustomerException, LogHandlerException {
		c_code = pickCode;
		mobile = wrongMobile;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a PickUpCustomer with an excess mobile.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreatePickUpCustomerExcessMobile() throws CustomerException, LogHandlerException {
		c_code = pickCode;
		mobile = excessMobile;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a PickUpCustomer with a whitespace mobile.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreatePickUpCustomerWhiteSpaceMobile() throws CustomerException, LogHandlerException {
		c_code = pickCode;
		mobile = whiteMobile;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a PickUpCustomer with a mobile of incorrect digit
	 * length.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreatePickUpCustomerInsufficientMobile() throws CustomerException, LogHandlerException {
		c_code = pickCode;
		mobile = shortMobile;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a PickUpCustomer with a mobile of no digits.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreatePickUpCustomerNoDigitsMobile() throws CustomerException, LogHandlerException {
		c_code = pickCode;
		mobile = alphaMobile;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a PickUpCustomer with no location x.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreatePickUpCustomerNoLocationX() throws CustomerException, LogHandlerException {
		c_code = pickCode;
		x_location = nothing;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a PickUpCustomer with a negative excess location x.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreatePickUpCustomerNegExcessLocationX() throws CustomerException, LogHandlerException {
		c_code = pickCode;
		x_location = "-1";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a PickUpCustomer with an excess location x.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreatePickUpCustomerExcessLocationX() throws CustomerException, LogHandlerException {
		c_code = pickCode;
		x_location = "1";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a PickUpCustomer with no location y.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testCreatePickUpCustomerNoLocationY() throws CustomerException, LogHandlerException {
		c_code = pickCode;
		y_location = nothing;
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a PickUpCustomer with a negative excess location y.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreatePickUpCustomerNegExcessLocationY() throws CustomerException, LogHandlerException {
		c_code = pickCode;
		y_location = "-1";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a PickUpCustomer with an excess location y.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreatePickUpCustomerExcessLocationY() throws CustomerException, LogHandlerException {
		c_code = pickCode;
		y_location = "1";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a PickUpCustomer with a negative excess distance.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreatePickUpCustomerNegExcess() throws CustomerException, LogHandlerException {
		c_code = pickCode;
		x_location = "-1";
		y_location = "-1";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test creation of a PickUpCustomer with an excess distance.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = CustomerException.class)
	public void testCreatePickUpCustomerExcessDistance() throws CustomerException, LogHandlerException {
		c_code = pickCode;
		x_location = "1";
		y_location = "1";
		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test createCustomer with a customer code containing numbers
	 *
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = CustomerException.class)
	public void invalidCustomerCodeCreateCustomerTest() throws LogHandlerException, CustomerException {
		c_code = "DV1";

		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test createCustomer with a locationX containing letters
	 *
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = LogHandlerException.class)
	public void invalidLocationXCreateCustomerTest() throws LogHandlerException, CustomerException {
		x_location = "0x";

		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test createCustomer with a locationY containing letters
	 *
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = LogHandlerException.class)
	public void invalidLocationYCreateCustomerTest() throws LogHandlerException, CustomerException {
		y_location = "0x";

		sentence = orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + "," + x_location + ","
				+ y_location + "," + p_code + "," + quantity;

		LogHandler.createCustomer(sentence);
	}

	/**
	 * Test population of the Customers field with a valid dataset.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test
	public void testPopulateCustomerDataset() throws CustomerException, LogHandlerException {
		file = new File("20170101.txt");
		LogHandler.populateCustomerDataset("logs\\20170101.txt");
	}

	/**
	 * Test population of the Customers field with an incorrect file format.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testPopulateCustomerDatasetInvalidFormat() throws CustomerException, LogHandlerException {
		file = new File("logs\\tests\\BadFormat.txt ");
		LogHandler.populateCustomerDataset(file.getAbsolutePath());
	}

	/**
	 * Test population of the Customers field with no file name.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testPopulateCustomerDatasetNoFilename() throws CustomerException, LogHandlerException {
		LogHandler.populateCustomerDataset(nothing);
	}

	/**
	 * Test population of the Customers field with an illegal filename.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testPopulateCustomerDatasetInvalidFilename() throws CustomerException, LogHandlerException {
		LogHandler.populateCustomerDataset("@#$%^&*(&^%.txt");
	}

	/**
	 * Test population of the Customers field with an impossible file name.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testPopulateCustomerDatasetImpossibleFilename() throws CustomerException, LogHandlerException {
		LogHandler.populateCustomerDataset(" .txt");
	}

	/**
	 * Test population of the Customers field with no file type.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testPopulateCustomerDatasetNoFileType() throws CustomerException, LogHandlerException {
		LogHandler.populateCustomerDataset("asdfg");
	}

	/**
	 * Test population of the Customers field with an empty file.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testPopulateCustomerDatasetEmptyFile() throws CustomerException, LogHandlerException {
		file = new File("logs\\tests\\EmptyFile.txt ");
		LogHandler.populateCustomerDataset(file.getAbsolutePath());
	}

	/**
	 * Test population of the Customers field with a file containing
	 * whitespaces.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */

	@Test(expected = LogHandlerException.class)
	public void testPopulateCustomerWhiteSpaceFile() throws CustomerException, LogHandlerException {
		file = new File("logs\\tests\\WhitespaceFile.txt ");
		LogHandler.populateCustomerDataset(file.getAbsolutePath());
	}

	/**
	 * Test createCustomerDataset with a file containing nothing
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = LogHandlerException.class)
	public void emptyFilePopulateCustomerDatasetTest() throws CustomerException, LogHandlerException {
		file = new File("logs\\tests\\EmptyFile.txt");
		LogHandler.populateCustomerDataset(file.getAbsolutePath());
	}

}
