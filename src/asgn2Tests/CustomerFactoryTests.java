package asgn2Tests;

import org.junit.Test;

import asgn2Customers.CustomerFactory;
import asgn2Exceptions.CustomerException;

/**
 * A class the that tests the asgn2Customers.CustomerFactory class.
 * 
 * @author Sylvester Soetianto
 *
 */
public class CustomerFactoryTests {
	private final String driverCode = "DVC", driverSimilar = "dvc", driverExcess = "DVCC", droneCode = "DNC",
			droneSimilar = "dnc", pickCode = "PUC", pickSimilar = "puc", whiteCode = "   ", customerName = "Dummy",
			nothing = "", customerMobile = "0123456789";
	private final Integer x = 1, zero_x = 0, y = 1, zero_y = 0;
	private String customerCode = driverCode;
	private final String name = customerName;
	private final String mobileNumber = customerMobile;
	private Integer locationX = x, locationY = y;

	/**
	 * Tests retrieval of a DriverDeliveryCustomer.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testGetDriverDeliveryCustomer() throws CustomerException {
		CustomerFactory.getCustomer(customerCode, name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests retrieval of a DroneDeliveryCustomer.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testGetDroneDeliveryCustomer() throws CustomerException {
		customerCode = droneCode;

		CustomerFactory.getCustomer(customerCode, name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests retrieval of a PickUpCustomer.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test
	public void testGetPickUpCustomer() throws CustomerException {
		locationX = zero_x;
		locationY = zero_y;
		customerCode = pickCode;

		CustomerFactory.getCustomer(customerCode, name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests retrieval of a customer with an invalid customer code.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testGetCustomerInvalidCustomerCode() throws CustomerException {
		customerCode = nothing;

		CustomerFactory.getCustomer(customerCode, name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests retrieval of a DriverDeliveryCustomer with a similar customer code.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testGetDriverDeliveryCustomerInvalidSimilarCustomerCode() throws CustomerException {
		customerCode = driverSimilar;

		CustomerFactory.getCustomer(customerCode, name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests retrieval of a DroneDeliveryCustomer with a similar customer code.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testGetDroneDeliveryCustomerInvalidSimilarCustomerCode() throws CustomerException {
		customerCode = droneSimilar;

		CustomerFactory.getCustomer(customerCode, name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests retrieval of a PickUpCustomer with a similar customer code.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testGePickUpCustomerInvalidSimilarCustomerCode() throws CustomerException {
		locationX = zero_x;
		locationY = zero_y;
		customerCode = pickSimilar;

		CustomerFactory.getCustomer(customerCode, name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests retrieval of a customer with an invalid customer code.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testGetCustomerInvalidCustomerCodeWhiteSpaces() throws CustomerException {
		customerCode = whiteCode;

		CustomerFactory.getCustomer(customerCode, name, mobileNumber, locationX, locationY);
	}

	/**
	 * Tests retrieval of a DriverDeliveryCustomer with an invalid customer code
	 * length.
	 *
	 * @throws CustomerException
	 *             if supplied parameters are invalid
	 */

	@Test(expected = CustomerException.class)
	public void testGetCustomerInvalidCustomerCodeLength() throws CustomerException {
		customerCode = driverExcess;

		CustomerFactory.getCustomer(customerCode, name, mobileNumber, locationX, locationY);
	}
}
