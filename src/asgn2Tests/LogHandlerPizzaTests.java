package asgn2Tests;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Restaurant.LogHandler;

/**
 * A class that tests the methods relating to the creation of Pizza objects in
 * the asgn2Restaurant.LogHander class.
 * 
 * @author Charles Alleman
 * 
 */
public class LogHandlerPizzaTests {
	/*
	 * Exception Testing
	 */

	private File file;

	private final String margherita = "PZM";
	private final String vegetarian = "PZV";
	private final String meatlovers = "PZL";
	private final String customerCode = "PUC";
	private String orderTime = "19:00:00";
	private String deliverTime = "19:20:00";
	private String name = "Casey Jones";
	private String mobile = "0123456789";
	private String c_code = "DVC";
	private String x_location = "5";
	private String y_location = "5";
	private String p_code;
	private String quantity;

	@Before
	public void resetValues() {
		orderTime = "19:00:00";
		deliverTime = "19:20:00";
		name = "Casey Jones";
		mobile = "0123456789";
		c_code = "DVC";
		x_location = "5";
		y_location = "5";
		p_code = margherita;
		quantity = "1";
	}

	/**
	 * Test createPizza with a pizza of zero quantity
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = PizzaException.class)
	public void zeroQuantityCreatePizzaTest() throws PizzaException, LogHandlerException {
		p_code = vegetarian;
		quantity = "0";

		LogHandler.createPizza(orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + ","
				+ x_location + "," + y_location + "," + p_code + "," + quantity);
	}

	/**
	 * Test createPizza with a pizza of negative quantity
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = PizzaException.class)
	public void negativeQuantityCreatePizzaTest() throws PizzaException, LogHandlerException {
		p_code = vegetarian;
		quantity = "-1";

		LogHandler.createPizza(orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + ","
				+ x_location + "," + y_location + "," + p_code + "," + quantity);
	}

	/**
	 * Test createPizza with a pizza of excess quantity
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = PizzaException.class)
	public void excessQuantityCreatePizzaTest() throws PizzaException, LogHandlerException {
		p_code = vegetarian;
		quantity = "11";

		LogHandler.createPizza(orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + ","
				+ x_location + "," + y_location + "," + p_code + "," + quantity);
	}

	/**
	 * Test createPizza with a pizza code with the same number of characters as
	 * a normal code
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = PizzaException.class)
	public void sameSizeCodeCreatePizzaTest() throws PizzaException, LogHandlerException {
		p_code = "ABC";
		quantity = "5";

		LogHandler.createPizza(orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + ","
				+ x_location + "," + y_location + "," + p_code + "," + quantity);
	}

	/**
	 * Test createPizza with a pizza code with a similar value to a normal code
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = PizzaException.class)
	public void customerCodeCreatePizzaTest() throws PizzaException, LogHandlerException {
		p_code = customerCode;
		quantity = "5";

		LogHandler.createPizza(orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + ","
				+ x_location + "," + y_location + "," + p_code + "," + quantity);
	}

	/**
	 * Test createPizza with a blank pizza code
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = LogHandlerException.class)
	public void emptyCodeCreatePizzaTest() throws PizzaException, LogHandlerException {
		p_code = "";
		quantity = "5";

		LogHandler.createPizza(orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + ","
				+ x_location + "," + y_location + "," + p_code + "," + quantity);
	}

	/**
	 * Test createPizza with a blank quantity value
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = LogHandlerException.class)
	public void emptyQuantityCreatePizzaTest() throws PizzaException, LogHandlerException {
		p_code = vegetarian;
		quantity = "";

		LogHandler.createPizza(orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + ","
				+ x_location + "," + y_location + "," + p_code + "," + quantity);
	}

	/**
	 * Test createPizza with a lower case pizza code
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = PizzaException.class)
	public void caseSensitivityCodeCreatePizzaTest() throws PizzaException, LogHandlerException {
		p_code = vegetarian.toLowerCase();
		quantity = "5";

		LogHandler.createPizza(orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + ","
				+ x_location + "," + y_location + "," + p_code + "," + quantity);
	}

	/**
	 * Test createPizza with an invalid pizza code
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = PizzaException.class)
	public void invalidCodeCreatePizzaTest() throws PizzaException, LogHandlerException {
		p_code = "AHVBJAS";
		quantity = "5";

		LogHandler.createPizza(orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + ","
				+ x_location + "," + y_location + "," + p_code + "," + quantity);
	}

	/**
	 * Test createPizza with an incorrectly formatted data entry
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = LogHandlerException.class)
	public void badFormatCreatePizzaTest() throws PizzaException, LogHandlerException {
		p_code = vegetarian;
		quantity = "5";

		LogHandler.createPizza(orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + ","
				+ x_location + "," + y_location + "," + p_code + "," + quantity + ", kjhsdfkjhsaddkjhsadf");
	}

	/**
	 * Test createPizza with a quantity containing non-numeric characters
	 * 
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = LogHandlerException.class)
	public void invalidQuantityCreatePizzaTest() throws LogHandlerException, PizzaException {
		p_code = vegetarian;
		quantity = "1h0";

		LogHandler.createPizza(orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + ","
				+ x_location + "," + y_location + "," + p_code + "," + quantity);
	}

	/**
	 * Test createPizzaDataset with a pizza of zero quantity
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = PizzaException.class)
	public void zeroQuantityPopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		file = new File("logs\\tests\\ZeroQuantity.txt");
		LogHandler.populatePizzaDataset(file.getAbsolutePath());
	}

	/**
	 * Test createPizzaDataset with a pizza of negative quantity
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = PizzaException.class)
	public void negativeQuantityPopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		file = new File("logs\\tests\\NegativeQuantity.txt");
		LogHandler.populatePizzaDataset(file.getAbsolutePath());
	}

	/**
	 * Test createPizzaDataset with a pizza of excess quantity
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = PizzaException.class)
	public void excessQuantityPopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		file = new File("logs\\tests\\ExcessQuantity.txt");
		LogHandler.populatePizzaDataset(file.getAbsolutePath());
	}

	/**
	 * Test createPizzaDataset with a pizza that has an empty quantity value
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = LogHandlerException.class)
	public void emptyQuantityPopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		file = new File("logs\\tests\\EmptyQuantity.txt");
		LogHandler.populatePizzaDataset(file.getAbsolutePath());
	}

	/**
	 * Test createPizzaDataset with a pizza code with the same size as a normal
	 * code
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = PizzaException.class)
	public void sameSizeCodePopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		file = new File("logs\\tests\\SameSizePizzaCode.txt");
		LogHandler.populatePizzaDataset(file.getAbsolutePath());
	}

	/**
	 * Test createPizzaDataset with a pizza code using a customer code
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = PizzaException.class)
	public void customerCodePopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		file = new File("logs\\tests\\CustomerPizzaCode.txt");
		LogHandler.populatePizzaDataset(file.getAbsolutePath());
	}

	/**
	 * Test createPizzaDataset with a pizza code with a lower case pizza code
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = PizzaException.class)
	public void caseSensitivityCodePopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		file = new File("logs\\tests\\CaseSensitivityPizzaCode.txt");
		LogHandler.populatePizzaDataset(file.getAbsolutePath());
	}

	/**
	 * Test createPizzaDataset with a pizza code with an empty pizza code
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = LogHandlerException.class)
	public void emptyCodePopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		file = new File("logs\\tests\\EmptyPizzaCode.txt");
		LogHandler.populatePizzaDataset(file.getAbsolutePath());
	}

	/**
	 * Test createPizzaDataset with an empty file path
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = LogHandlerException.class)
	public void noFilePopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		LogHandler.populatePizzaDataset("NotAFile");
	}

	/**
	 * Test createPizzaDataset with a incorrectly formatted file
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = LogHandlerException.class)
	public void badFormatPopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		file = new File("logs\\tests\\BadFormatPizzaCode.txt");
		LogHandler.populatePizzaDataset(file.getAbsolutePath());
	}

	/**
	 * Test createPizzaDataset with an impossible file name using spaces
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = LogHandlerException.class)
	public void whitespacePathPopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		LogHandler.populatePizzaDataset("   .txt");
	}

	/**
	 * Test createPizzaDataset with an impossible file name
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = LogHandlerException.class)
	public void wrongSymbolsPathPopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		LogHandler.populatePizzaDataset("@#$%^&*(&^%.txt");
	}

	/**
	 * Test createPizzaDataset with a file containing whitespace
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = LogHandlerException.class)
	public void whitespaceFilePopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		file = new File("logs\\tests\\WhitespaceFile.txt");
		LogHandler.populatePizzaDataset(file.getAbsolutePath());
	}

	/**
	 * Test createPizzaDataset with a file missing .txt
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = LogHandlerException.class)
	public void missingTXTFilePopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		file = new File("logs\\tests\\EmptyFile");
		LogHandler.populatePizzaDataset(file.getAbsolutePath());
	}

	/**
	 * Test createPizzaDataset with an empty file name
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = LogHandlerException.class)
	public void emptyFileNamePopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		LogHandler.populatePizzaDataset("");
	}

	/**
	 * Test populatePizzaDataset with a quantity containing non-numeric
	 * characters
	 * 
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = LogHandlerException.class)
	public void invalidQuantityPopulatePizzaDatasetTest() throws LogHandlerException, PizzaException {
		file = new File("logs\\tests\\InvalidQuantity.txt");
		LogHandler.populatePizzaDataset(file.getAbsolutePath());
	}

	/**
	 * Test populatePizzaDataset with an improper file type characters
	 * 
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	@Test(expected = LogHandlerException.class)
	public void improperFilePopulatePizzaDatasetTest() throws LogHandlerException, PizzaException {
		file = new File("logs\\tests\\badfile.jpeg");
		LogHandler.populatePizzaDataset(file.getAbsolutePath());
	}

	/**
	 * Test createPizzaDataset with a file containing nothing
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test(expected = LogHandlerException.class)
	public void emptyFilePopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		file = new File("logs\\tests\\EmptyFile.txt");
		LogHandler.populatePizzaDataset(file.getAbsolutePath());
	}

	/*
	 * Non Exception Testing
	 */

	/**
	 * Test createPizza with a pizza of one quantity
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test
	public void oneQuantityCreatePizzaTest() throws PizzaException, LogHandlerException {
		p_code = vegetarian;
		quantity = "1";

		LogHandler.createPizza(orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + ","
				+ x_location + "," + y_location + "," + p_code + "," + quantity);
	}

	/**
	 * Test createPizza with multiple pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test
	public void multipleQuantityCreatePizzaTest() throws PizzaException, LogHandlerException {
		p_code = vegetarian;
		quantity = "5";

		LogHandler.createPizza(orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + ","
				+ x_location + "," + y_location + "," + p_code + "," + quantity);
	}

	/**
	 * Test createPizza with the max amount of pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test
	public void maxQuantityCreatePizzaTest() throws PizzaException, LogHandlerException {
		p_code = vegetarian;
		quantity = "10";

		LogHandler.createPizza(orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + ","
				+ x_location + "," + y_location + "," + p_code + "," + quantity);
	}

	/**
	 * Test createPizza with a vegetarian pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test
	public void vegetarianCreatePizzaTest() throws PizzaException, LogHandlerException {
		p_code = vegetarian;
		quantity = "1";

		LogHandler.createPizza(orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + ","
				+ x_location + "," + y_location + "," + p_code + "," + quantity);
	}

	/**
	 * Test createPizza with a meatlovers pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test
	public void meatLoversCreatePizzaTest() throws PizzaException, LogHandlerException {
		p_code = meatlovers;
		quantity = "1";

		LogHandler.createPizza(orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + ","
				+ x_location + "," + y_location + "," + p_code + "," + quantity);
	}

	/**
	 * Test createPizza with a margherita pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test
	public void margheritaCreatePizzaTest() throws PizzaException, LogHandlerException {
		p_code = margherita;
		quantity = "1";

		LogHandler.createPizza(orderTime + "," + deliverTime + "," + name + "," + mobile + "," + c_code + ","
				+ x_location + "," + y_location + "," + p_code + "," + quantity);
	}

	/**
	 * Test createPizzaDataset with a pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test
	public void oneQuantityPopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		file = new File("logs\\tests\\OneQuantity.txt");
		LogHandler.populatePizzaDataset(file.getAbsolutePath());
	}

	/**
	 * Test createPizzaDataset with multiple pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test
	public void multipleQuantityPopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		file = new File("logs\\tests\\MultipleQuantity.txt");
		LogHandler.populatePizzaDataset(file.getAbsolutePath());
	}

	/**
	 * Test createPizzaDataset with a max number of pizzas
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test
	public void maxQuantityPopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		file = new File("logs\\tests\\MaxQuantity.txt");
		LogHandler.populatePizzaDataset(file.getAbsolutePath());
	}

	/**
	 * Test createPizzaDataset with a meatlovers pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test
	public void meatLoversPopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		file = new File("logs\\tests\\MeatLoversPizza.txt");
		LogHandler.populatePizzaDataset(file.getAbsolutePath());
	}

	/**
	 * Test createPizzaDataset with a margherita pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test
	public void margheritaPopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		file = new File("logs\\tests\\MargheritaPizza.txt");
		LogHandler.populatePizzaDataset(file.getAbsolutePath());
	}

	/**
	 * Test createPizzaDataset with a vegetarian pizza
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test
	public void vegetarianPopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		file = new File("logs\\tests\\VegetarianPizza.txt");
		LogHandler.populatePizzaDataset(file.getAbsolutePath());
	}

	/**
	 * Test createPizzaDataset with one order
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test
	public void onePopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		file = new File("logs\\tests\\OneOrder.txt");
		LogHandler.populatePizzaDataset(file.getAbsolutePath());
	}

	/**
	 * Test createPizzaDataset with multiple orders
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test
	public void multiplePopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		file = new File("logs\\tests\\MultipleOrder.txt");
		LogHandler.populatePizzaDataset(file.getAbsolutePath());
	}

	/**
	 * Test createPizzaDataset with a capital TXT deliminator
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test
	public void capitalisationPopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		file = new File("logs\\tests\\capitalisation.TXT");
		LogHandler.populatePizzaDataset(file.getAbsolutePath());
	}

	/**
	 * Test createPizzaDataset with random caps file type deliminator
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test
	public void randomCapPopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		file = new File("logs\\tests\\randomCap.tXt");
		LogHandler.populatePizzaDataset(file.getAbsolutePath());
	}

	/**
	 * Test createPizzaDataset with a different file type deliminator
	 * 
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 * @throws LogHandlerException
	 *             If there was a problem parsing the line from the log file.
	 */
	@Test
	public void differentDelimPopulatePizzaDatasetTest() throws PizzaException, LogHandlerException {
		file = new File("logs\\tests\\OneOrder.TXT");
		LogHandler.populatePizzaDataset(file.getAbsolutePath());
	}
}
